package ch.mobi.mobitor.plugin.zaproxy.domain;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.text.MessageFormat;

import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

public class ZaproxyInformation implements ApplicationInformation {

	public static final String ZAPROXY = "zaproxy";

	private final String projectKey;

	private String reportUrlTemplate;
	private Integer blockerNumber = 0;
	private Integer criticalNumber = 0;
	private Integer majorNumber = 0;
	private Integer minorNumber = 0;
	private Integer infoNumber = 0;
	private boolean informationUpdated = false;

	public ZaproxyInformation(String projectKey) {
		this.projectKey = projectKey;
		this.reportUrlTemplate = "";
	}

	public String getProjectKey() {
		return projectKey;
	}

	public String getWebUrl() {
		return MessageFormat.format(reportUrlTemplate, projectKey);
	}

	public Integer getBlockerNumber() {
		return blockerNumber;
	}

	public void setBlockerNumber(Integer blockerNumber) {
		this.blockerNumber = blockerNumber;
	}

	public Integer getCriticalNumber() {
		return criticalNumber;
	}

	public void setCriticalNumber(Integer criticalNumber) {
		this.criticalNumber = criticalNumber;
	}

	public Integer getMajorNumber() {
		return majorNumber;
	}

	public void setMajorNumber(Integer majorNumber) {
		this.majorNumber = majorNumber;
	}

	public Integer getMinorNumber() {
		return minorNumber;
	}

	public void setMinorNumber(Integer minorNumber) {
		this.minorNumber = minorNumber;
	}

	public Integer getInfoNumber() {
		return infoNumber;
	}

	public void setInfoNumber(Integer infoNumber) {
		this.infoNumber = infoNumber;
	}

	public boolean isInformationUpdated() {
		return informationUpdated;
	}

	public void setInformationUpdated(boolean informationUpdated) {
		this.informationUpdated = informationUpdated;
	}

	public Integer getHighestIssueNumber() {
		if (blockerNumber > 0) {
			return blockerNumber;
		}
		if (criticalNumber > 0) {
			return criticalNumber;
		}
		if (majorNumber > 0) {
			return majorNumber;
		}
		if (minorNumber > 0) {
			return minorNumber;
		}
		if (infoNumber > 0) {
			return infoNumber;
		}
		return 0;
	}

	public String getHighestIssueNumberMessage() {
		if (!isInformationUpdated()) {
			return "Data not available!";
		}
		if (blockerNumber > 0) {
			return blockerNumber.toString();
		}
		if (criticalNumber > 0) {
			return criticalNumber.toString();
		}
		if (majorNumber > 0) {
			return majorNumber.toString();
		}
		if (minorNumber > 0) {
			return minorNumber.toString();
		}
		if (infoNumber > 0) {
			return infoNumber.toString();
		}
		return "OK";
	}

	public String getIssueSummary() {
		if (!isInformationUpdated()) {
			return "Zaproxy data not available!";
		}
		StringBuffer sb = new StringBuffer("Issue numbers:");
		sb.append("    \"Blocker\": ").append(blockerNumber);
		sb.append("    \"Critical\": ").append(criticalNumber);
		sb.append("    \"Major\": ").append(majorNumber);
		sb.append("    \"Minor\": ").append(minorNumber);
		sb.append("    \"Info\": ").append(infoNumber);

		return sb.toString();
	}

	@Override
	public String getType() {
		return ZAPROXY;
	}

	@Override
	public boolean hasInformation() {
		return true;
	}
}

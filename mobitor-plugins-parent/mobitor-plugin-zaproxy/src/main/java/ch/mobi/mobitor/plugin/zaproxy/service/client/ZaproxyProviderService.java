package ch.mobi.mobitor.plugin.zaproxy.service.client;

/*-
 * §
 * mobitor-plugin-zaproxy
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ch.mobi.mobitor.plugin.zaproxy.client.domain.ProjectComponent;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.TaskInformation;
import ch.mobi.mobitor.plugin.zaproxy.client.domain.TaskList;
import ch.mobi.mobitor.plugin.zaproxy.domain.ZaproxyInformation;

@Service
public class ZaproxyProviderService {

	private static final Logger LOG = LoggerFactory.getLogger(ZaproxyProviderService.class);

	private final ZaproxySonarQubeClient zaproxySQClient;

	@Autowired
	public ZaproxyProviderService(ZaproxySonarQubeClient zaproxyClient) {
		this.zaproxySQClient = zaproxyClient;
	}

	public void updateZaproxyInformation(ZaproxyInformation zapInfo) {
		ProjectComponent projectComponent = zaproxySQClient.retrieveProjectInformation(zapInfo.getProjectKey());
		TaskList taskList = zaproxySQClient.retrieveTaskList(zapInfo.getProjectKey());

		if (projectComponent != null) {
			zapInfo.setBlockerNumber(projectComponent.getBlockers());
			zapInfo.setCriticalNumber(projectComponent.getCriticals());
			zapInfo.setMajorNumber(projectComponent.getMajors());
			zapInfo.setMinorNumber(projectComponent.getMinors());
			zapInfo.setInfoNumber(projectComponent.getInfos());
			zapInfo.setInformationUpdated(isAnalysisSuccessful(taskList));

			LOG.debug("updated zaproxy information for: " + zapInfo.getProjectKey());
		} else {
			LOG.warn("could not update zaproxy information for: " + zapInfo.getProjectKey());
		}
	}

	private boolean isAnalysisSuccessful(TaskList taskList) {
		if (taskList.isQueueEmpty() ) {
			return true;
		}
		else if (taskList.getCurrent()==null) {
			return true;
		}
		return taskList.getCurrent().isStatus(TaskInformation.Status.SUCCESS);
	}

}

package ch.mobi.mobitor.plugin.liima.service.client;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.LiimaClient;
import ch.mobi.mobitor.domain.deployment.Deployment;
import ch.mobi.mobitor.domain.deployment.DeploymentApplication;
import ch.mobi.mobitor.service.DeploymentInformationService;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import static ch.mobi.mobitor.plugin.liima.service.scheduling.LiimaInformationCollector.CACHE_NAME_LIIMA_DEPLOYMENTS;
import static java.text.MessageFormat.format;
import static java.util.stream.Collectors.toList;

@Service
public class LiimaInformationProviderService implements DeploymentInformationService {

    private final LiimaClient liimaClient;

    private static final Logger LOG = LoggerFactory.getLogger(LiimaInformationProviderService.class);

    @Autowired
    public LiimaInformationProviderService(LiimaClient liimaClient) {
        this.liimaClient = liimaClient;
    }

    @Nullable
    private List<ch.mobi.liima.client.dto.Deployment> retrieveLiimaDeployments(@NotNull String serverName) {
        LOG.debug("Retrieve latest LIIMA deployments for server: " + serverName);
        List<ch.mobi.liima.client.dto.Deployment> deployments = liimaClient.retrieveDeployments(serverName);
        return deployments;
    }

    @Override
    public List<Deployment> filterDeployments(List<Deployment> deploymentsForServer, Set<String> environments) {
        return deploymentsForServer.stream()
                                   .filter(deployment -> environments.contains(deployment.getEnvironment()))
                                   .collect(toList());
    }

    @Override
    public String getVersion(String applicationName, List<Deployment> deployments) {
        for (Deployment deployInfo : deployments) {
            if (!"success".equals(deployInfo.getState())) {
                LOG.warn(format("The last deployment of {0} was not successful!", deployInfo.getServerName()));
            }

            for (DeploymentApplication appWithVersion : deployInfo.getApplications()) {
                LOG.debug(format("Try to find version for given application. {0}:{1}", appWithVersion.getApplicationName(), appWithVersion.getVersion()));
                if (applicationName.equalsIgnoreCase(appWithVersion.getApplicationName())) {
                    LOG.debug("Found version for " + applicationName);
                    return appWithVersion.getVersion();
                }
            }
        }
        return null;
    }

    @Cacheable(cacheNames = CACHE_NAME_LIIMA_DEPLOYMENTS)
    // retrieve all environments, the same server might be on different screens with different environments
    @Override
    public List<Deployment> retrieveDeployments(@NotNull String serverName) {
        List<ch.mobi.liima.client.dto.Deployment> liimaDeployments = retrieveLiimaDeployments(serverName);

        List<Deployment> deployments = new ArrayList<>();
        if (liimaDeployments != null) {
            liimaDeployments.forEach(liimaDeployment -> {
                LiimaDeploymentAssembler assembler = new LiimaDeploymentAssembler(liimaDeployment);
                deployments.add(assembler.getDeployment());
            });
        }

        return deployments;
    }

    @Override
    public String createDeploymentWebUri(String serverName, String applicationName, String environment) {
        return liimaClient.createDeploymentsWebUri(serverName, applicationName, environment);
    }
}

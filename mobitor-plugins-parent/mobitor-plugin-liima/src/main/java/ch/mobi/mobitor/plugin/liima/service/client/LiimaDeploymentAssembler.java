package ch.mobi.mobitor.plugin.liima.service.client;

/*-
 * §
 * mobitor-plugin-liima
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.liima.client.dto.AppWithVersion;
import ch.mobi.liima.client.dto.Deployment;
import ch.mobi.liima.client.dto.DeploymentParameter;
import ch.mobi.mobitor.domain.deployment.DeploymentApplication;

import java.util.List;
import java.util.Optional;

public class LiimaDeploymentAssembler {

    private final Deployment liimaDeployment;

    public LiimaDeploymentAssembler(Deployment liimaDeployment) {
        this.liimaDeployment = liimaDeployment;
    }

    public ch.mobi.mobitor.domain.deployment.Deployment getDeployment() {
        ch.mobi.mobitor.domain.deployment.Deployment deployment = new ch.mobi.mobitor.domain.deployment.Deployment();

        deployment.setServerName(liimaDeployment.getAppServerName());
        deployment.setDeploymentDate(liimaDeployment.getDeploymentDate());
        deployment.setEnvironment(liimaDeployment.getEnvironmentName());
        deployment.setId(String.valueOf(liimaDeployment.getTrackingId()));
        deployment.setState(liimaDeployment.getState());
        deployment.setRequestUser(liimaDeployment.getRequestUser());

        List<AppWithVersion> appsWithVersion = liimaDeployment.getAppsWithVersion();
        Optional.ofNullable(appsWithVersion).ifPresent(list -> list.forEach(appWithVersion -> {
            DeploymentApplication deploymentApplication = new DeploymentApplication();
            deploymentApplication.setApplicationName(appWithVersion.getApplicationName());
            deploymentApplication.setVersion(appWithVersion.getVersion());
            deployment.addDeploymentApplication(deploymentApplication);
        }));

        List<DeploymentParameter> deploymentParameters = liimaDeployment.getDeploymentParameters();
        Optional.ofNullable(deploymentParameters).ifPresent(list -> list.forEach(deploymentParameter -> {
            String key = deploymentParameter.getKey();
            String value = deploymentParameter.getValue();
            deployment.addDeploymentParameter(key, value);
        }));

        return deployment;
    }
}

package ch.mobi.mobitor.plugin.nexusiq.service.client;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.nexusiq.NexusIqPluginConfiguration;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationViolationResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationViolationsResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationsResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.PolicyViolationResponse;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class NexusIqClientTest {

	public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

	private NexusIqClient client;

	@BeforeAll
	static void setup() {
		wiremock.start();
	}

	@BeforeEach
	void setupEach() {
		String baseUrl = "http://localhost:" + wiremock.port();
		NexusIqPluginConfiguration nexusIqConfiguration = new NexusIqPluginConfiguration();
		nexusIqConfiguration.setBaseUrl(baseUrl);
		nexusIqConfiguration.setUsername("junit");
		nexusIqConfiguration.setPassword("junit");

		client = new NexusIqClient(nexusIqConfiguration);
	}

	@AfterEach
	void after() {
		wiremock.resetAll();
	}

	@AfterAll
	static void clean() {
		wiremock.shutdown();
	}

	@Test
	public void retrieveViolations() throws Exception {
		// arrange
		wiremock.stubFor(get(urlMatching("/api/v2/policyViolations?(.*)"))
								 .withQueryParam("p", matching("([a-z0-9]+)"))
								 .withHeader("Accept", equalTo("application/json"))
								 .willReturn(aResponse()
													 .withStatus(200)
													 .withHeader("Content-Type", "application/json")
													 .withBodyFile("nexus-iq-response-policy-violations.json")));

		List<String> violationIds = asList("d89b440a4a65481b8109541bd1673910", "eb17003085e84a8cad0fea7d312f3fbc");

		// act
		ApplicationViolationsResponse applicationViolations = client.retrieveViolations(violationIds);

		// assert
		assertThat(applicationViolations).isNotNull();
		assertThat(applicationViolations.getApplicationViolations()).hasSizeGreaterThan(0);

		for (ApplicationViolationResponse applicationViolationResponse : applicationViolations.getApplicationViolations()) {
			for (PolicyViolationResponse policyViolationResponse : applicationViolationResponse.getPolicyViolations()) {
				assertThat(policyViolationResponse.getStage()).isNotNull();
			}
		}
	}

	@Test
	public void retrieveApplications() throws Exception {
		// arrange
		wiremock.stubFor(get(urlMatching("/api/v2/applications(.*)"))
								 .withHeader("Accept", equalTo("application/json"))
								 .willReturn(aResponse()
													 .withStatus(200)
													 .withHeader("Content-Type", "application/json")
													 .withBodyFile("nexus-iq-response-applications.json")));

		// act
		ApplicationsResponse applications = client.retrieveApplications();

		// assert
		assertThat(applications).isNotNull();
		assertThat(applications.getApplications()).hasSizeGreaterThan(0);
		assertTrue(applications.hasApplication("mobitor"));
		assertFalse(applications.hasApplication("does-not_exist"));
	}

}

package ch.mobi.mobitor.plugin.nexusiq.service.client;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.nexusiq.NexusIqPluginConfiguration;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationViolationsResponse;
import ch.mobi.mobitor.plugin.nexusiq.service.client.domain.ApplicationsResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.List;

@Component
public class NexusIqClient {

    private static final Logger LOG = LoggerFactory.getLogger(NexusIqClient.class);

    private static final String NEXUS_IQ_REST_PATH = "/api/v2";

    private final NexusIqPluginConfiguration nexusIqPluginConfiguration;

    @Autowired
    public NexusIqClient(NexusIqPluginConfiguration nexusIqPluginConfiguration) {
        this.nexusIqPluginConfiguration = nexusIqPluginConfiguration;
    }
    
    public String getBaseUrl() {
    	return this.nexusIqPluginConfiguration.getBaseUrl();
    }

    public ApplicationViolationsResponse retrieveViolations(List<String> violationIds) throws NexusIqUnavailableException {
        StringBuilder sb = new StringBuilder();
        for (String violationId : violationIds) {
            sb.append("p=").append(violationId).append("&");
        }
        LOG.debug("Retrieve violations for id's: " + sb.toString());

        try {
            String response = getResponse("/policyViolations?", sb.toString());
            return createViolationsResponse(response);
        } catch (IOException e) {
            throw new NexusIqUnavailableException("Could not retrieve violations from Nexus IQ!", e);
        }
    }

    public ApplicationsResponse retrieveApplications() throws NexusIqUnavailableException {
        LOG.debug("Retrieve applications");
        try {
            String response = getResponse("/applications", null);
            return createApplicationsResponse(response);
        } catch (IOException e) {
            throw new NexusIqUnavailableException("Could not retrieve applications from Nexus IQ!", e);
        }
    }

    private String getResponse(String rest, String params) throws IOException {
        String nexusIqHost = nexusIqPluginConfiguration.getBaseUrl();
        StringBuilder requestUrl = new StringBuilder(nexusIqHost);
        requestUrl.append(NEXUS_IQ_REST_PATH);
        requestUrl.append(rest);
        if (params != null) {
            requestUrl.append(params);
        }

        HttpHost host = HttpHost.create(nexusIqHost);
        String username = nexusIqPluginConfiguration.getUsername();
        String password = nexusIqPluginConfiguration.getPassword();
        int timeout = nexusIqPluginConfiguration.getTimeout();

        Executor executor = Executor.newInstance();
        executor.auth(host, username, password).authPreemptive(host);

        LOG.debug("Executing request to: " + requestUrl.toString());
        return executor
            .execute(Request
                .Get(requestUrl.toString())
                .addHeader("accept", "application/json")
                .connectTimeout(3000)
                .socketTimeout(timeout))
            .returnContent()
            .asString();
    }

    private ApplicationViolationsResponse createViolationsResponse(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        return mapper.readValue(response, ApplicationViolationsResponse.class);
    }

    private ApplicationsResponse createApplicationsResponse(String response) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        return mapper.readValue(response, ApplicationsResponse.class);
    }

}

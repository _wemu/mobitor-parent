package ch.mobi.mobitor.plugin.nexusiq.service.client.domain;

/*-
 * §
 * mobitor-plugin-nexusiq
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class ApplicationsResponse {

    private static final Logger LOG = LoggerFactory.getLogger(ApplicationsResponse.class);

    @JsonProperty
    private List<ApplicationResponse> applications;

    public List<ApplicationResponse> getApplications() {
        return applications;
    }

    public void setApplications(List<ApplicationResponse> applications) {
        this.applications = applications;
    }

    public boolean hasApplication(String publicId) {
        if (applications == null) {
            LOG.error("applications not initialized");
            return false;
        }
        if (publicId == null) {
            LOG.error("Parameter publicId may not be null!");
            return false;
        }
        for (ApplicationResponse resp : applications) {
            if (publicId.equals(resp.getPublicId())) {
                return true;
            }
        }
        return false;
    }

}

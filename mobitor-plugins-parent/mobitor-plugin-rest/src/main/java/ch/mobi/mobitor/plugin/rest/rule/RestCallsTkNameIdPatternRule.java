package ch.mobi.mobitor.plugin.rest.rule;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.*;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;
import static org.apache.commons.lang3.StringUtils.isEmpty;


@Component
public class RestCallsTkNameIdPatternRule implements PipelineRule {

    private final String REGEX = "^(?<appName>[a-zA-Z][a-zA-Z0-9]{1,6})-(?<fkName>[a-zA-Z][a-zA-Z0-9]*)(-(?<tkName>[a-zA-Z][a-zA-Z0-9]*))?-(?<tkTypeName>[a-zA-Z]+)$";
    private final Pattern PATTERN = Pattern.compile(REGEX);

    @Override
    public void evaluateRule(Pipeline pipeline, RuleEvaluation re) {
        Map<String, ServerContext> serverContextMap = pipeline.getServerContextMap();
        for (Map.Entry<String, ServerContext> entry : serverContextMap.entrySet()) {
            String env = entry.getKey();
            ServerContext serverContext = entry.getValue();

            List<RestCallInformation> restCallInformationList = serverContext.getMatchingInformation(REST);

            restCallInformationList.stream()
                                   .filter(this::ruleViolated)
                                   .forEach(restInfo -> re.addViolation(env, restInfo, RuleViolationSeverity.ERROR, "tkNameId is invalid: " + restInfo.getTkNameId()));
        }
    }

    boolean ruleViolated(RestCallInformation restCallInformation) {
        String tkNameId = restCallInformation.getTkNameId();
        if(isEmpty(tkNameId) && isEmpty(restCallInformation.getSwaggerUri())){
            return false; //it is not possible to determine the tkNameId from the service, because it has no swagger

        } else if (tkNameId == null) {
            return true;
        }

        final Matcher matcher = PATTERN.matcher(tkNameId);
        boolean matches = matcher.matches();
        return !matches;
    }

    @Override
    public boolean validatesType(String type) {
        return REST.equals(type);
    }
}

package ch.mobi.mobitor.plugin.rest.service.scheduling;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import static ch.mobi.mobitor.plugin.rest.domain.RestCallInformation.REST;
import static ch.mobi.mobitor.plugin.rest.domain.RestInformationUtils.getMatchingPaths;
import static ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse.createErrorRestServiceResponse;
import static java.util.function.Predicate.not;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Predicate;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.http.client.HttpResponseException;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.RestResourcePlugin;
import ch.mobi.mobitor.plugin.rest.domain.RestCallAdditionalUri;
import ch.mobi.mobitor.plugin.rest.domain.RestCallInformation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.swagger.Server;
import ch.mobi.mobitor.plugin.rest.domain.swagger.ApiSpecResponse;
import ch.mobi.mobitor.plugin.rest.service.RestPluginConstants;
import ch.mobi.mobitor.plugin.rest.service.client.ApiSpecClient;
import ch.mobi.mobitor.plugin.rest.service.restservice.AdditionalEndpointInterpreter;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;

@Component
@ConditionalOnBean(RestResourcePlugin.class)
public class RestInformationCollector {

    private final static Logger LOG = LoggerFactory.getLogger(RestInformationCollector.class);

    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final RestCallMetricService restCallMetricService;
    private final CollectorMetricService collectorMetricService;
    private final ApiSpecClient apiSpecClient;
    private final List<SwaggerEndpointInterpreter> swaggerEndpointInterpreters;
    private final List<AdditionalEndpointInterpreter> additionalEndpointInterpreters;
    private final RestPluginConfiguration restPluginConfiguration;

    @Autowired
    public RestInformationCollector(ScreensModel screensModel, RuleService ruleService, RestCallMetricService restCallMetricService,
            CollectorMetricService collectorMetricService, ApiSpecClient apiSpecClient,
            List<SwaggerEndpointInterpreter> swaggerEndpointInterpreters,
            List<AdditionalEndpointInterpreter> additionalEndpointInterpreters, RestPluginConfiguration restPluginConfiguration) {
        this.screensModel = screensModel;
        this.ruleService = ruleService;
        this.restCallMetricService = restCallMetricService;
        this.collectorMetricService = collectorMetricService;
        this.apiSpecClient = apiSpecClient;
        this.swaggerEndpointInterpreters = swaggerEndpointInterpreters;
        this.additionalEndpointInterpreters = additionalEndpointInterpreters;
        this.restPluginConfiguration = restPluginConfiguration;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.restServicePollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.first}")
    @CacheEvict(cacheNames = RestPluginConstants.CACHE_NAME_REST_RESPONSES, allEntries = true)
    public void collectRestCallsServiceResponses() {
        long startTime = System.currentTimeMillis();
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateRestServiceStatusFromSwagger);
        long duration = System.currentTimeMillis() - startTime;
        LOG.info("Polling REST services took: " + duration + "ms");
        collectorMetricService.submitCollectorDuration("rest", duration);
        collectorMetricService.updateLastRunCompleted(REST);
    }

    private void populateRestServiceStatusFromSwagger(Screen screen) {
        long startTime = System.currentTimeMillis();
        List<RestCallInformation> restInfoList = screen.getMatchingInformation(REST);
        restInfoList.forEach(this::populateRestCallInformation);
        long duration = System.currentTimeMillis() - startTime;
        LOG.debug("Refreshing REST status for screen '" + screen.getLabel() + "' took: " + duration + "ms");
        screen.setRefreshDate(REST, new Date());
        ruleService.updateRuleEvaluation(screen, REST);
    }

    private void populateRestCallInformation(RestCallInformation restCallInformation) {
        List<RestServiceResponse> allRestResponses = new ArrayList<>();
        // load swagger.json / openapi.yaml
        String apiSpecUri = restCallInformation.getSwaggerUri();
        ApiSpecResponse apiSpecResponse = null;
        if (isNotEmpty(apiSpecUri)) {
            try {
                apiSpecResponse = apiSpecClient.retrieveApiSpec(apiSpecUri);
                String restBasePath = apiSpecClient.buildBaseUrl(apiSpecUri, getBasePath(apiSpecResponse, apiSpecUri));
                getAllRestPathsPerInterpreterAndCallItsMatchingMethods(restCallInformation, allRestResponses, apiSpecResponse,
                        restBasePath);
            } catch (Exception e) {
                LOG.debug("Failed to load API spec. uri=" + apiSpecUri, e);
                if (e instanceof HttpResponseException) {
                    HttpResponseException ex = (HttpResponseException) e;
                    RestServiceResponse swaggerErrorResponse =
                            createErrorRestServiceResponse(apiSpecUri, ex.getStatusCode(), ex.getReasonPhrase());
                    allRestResponses.add(swaggerErrorResponse);
                } else {
                    RestServiceResponse swaggerErrorResponse = createErrorRestServiceResponse(apiSpecUri, 444,
                            "Could not load API spec due to '" + e.getMessage() + "'");
                    allRestResponses.add(swaggerErrorResponse);
                }
            }
        }

        addAdditionalResponses(restCallInformation, allRestResponses);

        if (isNotEmpty(apiSpecUri)) {
            Set<String> tkNameIds = extractTkNameIds(allRestResponses);
            String tkNameId = tkNameIds.stream()
                    .findFirst()
                    .orElse(apiSpecResponse == null ? "!" + apiSpecUri : "!" + getBasePath(apiSpecResponse, apiSpecUri));
            restCallInformation.setTkNameId(tkNameId);
        }

        restCallInformation.setRestServiceResponses(allRestResponses);
        restCallInformation.setRefreshDate(new Date());
        LOG.debug("Number of REST paths found: " + allRestResponses.size());

        restCallMetricService.submitRestCallFailures(restCallInformation);
    }

    String getBasePath(ApiSpecResponse apiSpecResponse, String apiSpecUri) {
        String basePath;
        if (apiSpecResponse.getSwaggerVersion() != null) {
            basePath = apiSpecResponse.getBasePath();
        } else if (apiSpecResponse.getOpenApiVersion() != null) {
            if (apiSpecResponse.getServers() != null) {
                basePath = apiSpecResponse.getServers().stream().map(Server::getUrl).filter(not(Objects::isNull)).findFirst()
                        // In the OpenApi 3 format the server URL is complete and it is not only the basePath as in Swagger2 format
                        .map(s -> extractBasePathFromOpenApiServerProperty(s, apiSpecUri)).orElse(null);
            } else {
                // "servers" is not mandatory in OpenAPI 3.0
                basePath = deriveFallbackBasePath(apiSpecUri);
            }
        } else {
            LOG.debug("Invalid OpenAPI response: neither 'swaggerVersion' nor 'openApiVersion' is set");
            // assume Swagger/OpenAPI 2 for compatibility
            basePath = apiSpecResponse.getBasePath();
        }
        if (basePath == null) {
            basePath = "";
        }
        return basePath;
    }


    /**
     * In OpenAPI 3.0 the "servers" property is not mandatory and it is not generated by some generators. Thus - if not "servers" is
     * present in the API spec - we derive the basepath from the API spec uri (relative) and add the path-part "rest" as convention.
     *
     * @param apiSpecUri
     *         Example: https://example.test/foo/bar/openapi.yaml
     * @return Fallback base path. In this example: /foo/bar/rest
     */
    @Nullable
    private String deriveFallbackBasePath(String apiSpecUri) {
        String pathServingApiSpec = derivePathServingApiSpec(apiSpecUri);
        if (pathServingApiSpec == null) {
            return null;
        }
        return pathServingApiSpec + (restPluginConfiguration.getOpenApiDefaultServerAdditationalPath()
                                     == null ? "" : restPluginConfiguration.getOpenApiDefaultServerAdditationalPath());
    }

    @Nullable
    private String derivePathServingApiSpec(String apiSpecUri) {
        String basePath;
        try {
            URI uri = new URI(apiSpecUri);
            String path = uri.getPath();
            basePath = path.substring(0, path.lastIndexOf("/"));
        } catch (URISyntaxException e) {
            LOG.error("Could not derive basepath api spec uri: " + apiSpecUri, e);
            basePath = null;
        }
        return basePath;
    }

    String extractBasePathFromOpenApiServerProperty(String urlFromServerInApiSpec, String apiSpecUri) {
        String basePath;
        if (urlFromServerInApiSpec.contains("http")) {
            try {
                URL url = new URL(urlFromServerInApiSpec);
                basePath = url.getPath();
            } catch (MalformedURLException e) {
                basePath = urlFromServerInApiSpec;
            }
        } else {
            if (urlFromServerInApiSpec.startsWith("/")) {
                basePath = urlFromServerInApiSpec;
            } else {
                // According to the OpenAPI 3.0 spec, "url" may be relative.
                // => Relative to the location where the OpenAPI document is being served.
                String pathServingApiSpec = derivePathServingApiSpec(apiSpecUri);
                basePath = pathServingApiSpec + "/" + urlFromServerInApiSpec;
            }
        }
        return basePath;
    }

    private void getAllRestPathsPerInterpreterAndCallItsMatchingMethods(RestCallInformation restCallInformation,
            List<RestServiceResponse> allRestResponses, ApiSpecResponse apiSpecResponse, String restBasePath) {
        for (SwaggerEndpointInterpreter interpreter : swaggerEndpointInterpreters) {
            Predicate<String> matchPredicate = interpreter.getMatchPredicate();
            List<String> matchingPaths = getMatchingPaths(restBasePath, matchPredicate, apiSpecResponse.getPaths());

            matchingPaths.forEach(path -> {
                RestServiceResponse restServiceResponse = interpreter.fetchResponse(path);
                allRestResponses.add(restServiceResponse);

                Optional.ofNullable(restServiceResponse.getProperty("stackName")).ifPresent(restCallInformation::setStackName);
                Optional.ofNullable(restServiceResponse.getProperty("stackVersion"))
                        .ifPresent(restCallInformation::setStackVersion);
            });
        }
    }

    private void addAdditionalResponses(RestCallInformation restCallInformation, List<RestServiceResponse> allRestResponses) {
        List<RestCallAdditionalUri> additionalUris = restCallInformation.getAdditionalUris();
        if (CollectionUtils.isNotEmpty(additionalUris)) {
            for (AdditionalEndpointInterpreter interpreter : additionalEndpointInterpreters) {
                additionalUris.forEach(additionalUri -> {
                    RestServiceResponse restServiceResponse =
                            interpreter.fetchResponse(additionalUri.getUri(), additionalUri.getValidationRegex());
                    LOG.debug(restServiceResponse.toString());
                    allRestResponses.add(restServiceResponse);
                });
            }
        }
    }

    private Set<String> extractTkNameIds(List<RestServiceResponse> allSwaggerRestResponses) {
        Set<String> tkNameIds = new HashSet<>();
        allSwaggerRestResponses.forEach(restResponse -> {
            if (restResponse.getProperty("tkNameId") != null) {
                tkNameIds.add(restResponse.getProperty("tkNameId"));
            }
        });

        if (CollectionUtils.size(tkNameIds) > 1) {
            LOG.error("Different tkNameIds reported: " + tkNameIds);
        }
        return tkNameIds;
    }

}

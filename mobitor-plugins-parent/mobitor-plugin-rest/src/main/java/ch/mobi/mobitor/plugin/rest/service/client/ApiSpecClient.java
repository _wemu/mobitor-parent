package ch.mobi.mobitor.plugin.rest.service.client;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import ch.mobi.mobitor.plugin.rest.domain.swagger.ApiSpecResponse;
import org.apache.http.client.fluent.Content;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.message.BasicHeader;
import org.jetbrains.annotations.TestOnly;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import static ch.mobi.mobitor.plugin.rest.service.restservice.RequestHeaderConstants.*;

@Component
public class ApiSpecClient {

    private static final Logger LOG = LoggerFactory.getLogger(ApiSpecClient.class);

    private final RestPluginConfiguration restPluginConfiguration;
    private final String username;
    private final List<ApiSpecHandlingStrategy> apiSpecHandlingStrategies;
    private final String apiSecAcceptHeaderValue;

    @Autowired
    public ApiSpecClient(RestPluginConfiguration restPluginConfiguration,
                         @Value("${restService.authentification.username}") String username,
                         List<ApiSpecHandlingStrategy> apiSpecHandlingStrategies) {
        this.restPluginConfiguration = restPluginConfiguration;
        this.username = username;
        this.apiSpecHandlingStrategies = apiSpecHandlingStrategies;
        this.apiSecAcceptHeaderValue = this.apiSpecHandlingStrategies.stream()
                .map(ApiSpecHandlingStrategy::getAcceptHeaderValue)
                .collect(Collectors.joining(", "));
    }

    public ApiSpecResponse retrieveApiSpec(String apiSpecUri) throws IOException, URISyntaxException {
        LOG.debug("Fetching API spec via HTTP GET apiSpecUri=" + apiSpecUri);
        Content apiSpecContent = fetchApiSpec(apiSpecUri);
        LOG.trace("API spec response as string (UTF-8): " + apiSpecContent.asString(StandardCharsets.UTF_8));
        ApiSpecHandlingStrategy apiSpecHandlingStrategy = findHandlingStrategy(apiSpecContent);
        if (apiSpecHandlingStrategy == null) {
            // Throwing a runtime exception would abort the entire process. Thus using an IOException.
            throw new IOException("Unable to find a way to handle the retrieved API spec from " + apiSpecUri);
        }
        LOG.trace("Deserializing API spec. ApiSpecHandlingStrategy=" + apiSpecHandlingStrategy.getClass().getName());
        return apiSpecHandlingStrategy.deserializeResponse(apiSpecContent);
    }

    private ApiSpecHandlingStrategy findHandlingStrategy(Content apiSpecContent) {
        for (ApiSpecHandlingStrategy apiSpecHandlingStrategy : apiSpecHandlingStrategies) {
            if (apiSpecHandlingStrategy.canHandleContent(apiSpecContent)) {
                return apiSpecHandlingStrategy;
            }
        }
        return null;
    }

    @TestOnly
    Content fetchApiSpec(String apiSpecUri) throws IOException, URISyntaxException {
        try {
            return Executor.newInstance()
                    .execute(Request.Get(new URI(apiSpecUri))
                            .addHeader(new BasicHeader("Accept", apiSecAcceptHeaderValue))
                            .addHeader(HEADER_CALLER, HEADER_CALLER_VALUE)
                            .addHeader(HEADER_CALLER_VERSION, restPluginConfiguration.getVersion())
                            .addHeader(HEADER_CORR_ID, String.valueOf(UUID.randomUUID()))
                            .addHeader(HEADER_X_USER_ID, username)
                            .connectTimeout(10000)
                            .socketTimeout(10000))
                    .returnContent();
        } catch (IOException e) {
            LOG.error("Could not retrieve API spec from uri=" + apiSpecUri + ", reason=" + e.getMessage());
            throw e;
        } catch (URISyntaxException e) {
            LOG.error("Could not parse API spec uri: " + e.getMessage());
            throw e;
        }
    }

    /**
     * @param swaggerJsonUrl  full URL to the swagger.json ("https://app.host.domain/ovn/swagger.json")
     * @param swaggerBasePath the basePath from the swagger.json ("/ovn/rest")
     * @return the URL pointing to the /rest/ endpoints ("https://app.host.domain/ovn/rest") or null
     * if there is a parsing issue
     */
    public String buildBaseUrl(String swaggerJsonUrl, String swaggerBasePath) {
        try {
            URI uri = new URI(swaggerJsonUrl);
            String host = uri.getHost();
            String scheme = uri.getScheme();
            return scheme + "://" + host + swaggerBasePath;
        } catch (URISyntaxException e) {
            return null;
        }
    }
}

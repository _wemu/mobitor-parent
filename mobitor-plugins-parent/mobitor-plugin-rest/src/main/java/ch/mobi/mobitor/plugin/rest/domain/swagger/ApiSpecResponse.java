package ch.mobi.mobitor.plugin.rest.domain.swagger;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ApiSpecResponse {

    // OpenAPI 2 only (fka Swagger)
    @JsonProperty("swagger") private String swaggerVersion;
    @JsonProperty("basePath") private String basePath;

    // OpenAPI 3 only
    @JsonProperty("openapi") private String openApiVersion;
    @JsonProperty("servers") private List<Server> servers;

    // OpenAPI 2+
    @JsonProperty("paths") private Map<String, PathHttpMethods> paths;

    public ApiSpecResponse() {
    }

    public String getSwaggerVersion() {
        return swaggerVersion;
    }

    public void setSwaggerVersion(String swaggerVersion) {
        this.swaggerVersion = swaggerVersion;
    }

    public String getBasePath() {
        return basePath;
    }

    public void setBasePath(String basePath) {
        this.basePath = basePath;
    }

    public String getOpenApiVersion() {
        return openApiVersion;
    }

    public void setOpenApiVersion(String openApiVersion) {
        this.openApiVersion = openApiVersion;
    }

    public List<Server> getServers() {
        return servers;
    }

    public void setServers(List<Server> servers) {
        this.servers = servers;
    }

    public Map<String, PathHttpMethods> getPaths() {
        return paths;
    }

    public void setPaths(Map<String, PathHttpMethods> paths) {
        this.paths = paths;
    }
}

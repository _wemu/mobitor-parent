package ch.mobi.mobitor.plugin.rest.service.restservice.interpreters;

/*-
 * §
 * mobitor-plugin-rest
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation;
import ch.mobi.mobitor.plugin.rest.domain.RestServiceResponse;
import ch.mobi.mobitor.plugin.rest.domain.interpreters.healthstatus.HealthStatusResponse;
import ch.mobi.mobitor.plugin.rest.service.restservice.SwaggerEndpointInterpreter;
import ch.mobi.mobitor.plugin.rest.service.restservice.TkNameIdExtractor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.FAILURE;
import static ch.mobi.mobitor.plugin.rest.domain.ResponseInterpretation.SUCCESS;
import static ch.mobi.mobitor.plugin.rest.service.RestPluginConstants.CACHE_NAME_REST_RESPONSES;
import static org.apache.commons.lang3.StringUtils.isEmpty;

@Component
public class Jes2HealthStatusInterpreter implements SwaggerEndpointInterpreter, TkNameIdExtractor {

    private static final Logger LOG = LoggerFactory.getLogger(Jes2HealthStatusInterpreter.class);

    private final RestServiceHttpRequestExecutor restServiceHttpRequestExecutor;

    @Autowired
    public Jes2HealthStatusInterpreter(@Qualifier("authenticating") RestServiceHttpRequestExecutor restServiceHttpRequestExecutor) {
        this.restServiceHttpRequestExecutor = restServiceHttpRequestExecutor;
    }

    @Override
    public Predicate<String> getMatchPredicate() {
        return path -> path.contains("/health/status");
    }

    @Override
    @Cacheable(cacheNames = CACHE_NAME_REST_RESPONSES)
    public RestServiceResponse fetchResponse(String uri) {
        try {
            HttpResponse httpResponse = restServiceHttpRequestExecutor.execute(uri);
            int statusCode = httpResponse.getStatusLine().getStatusCode();
            String json = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
            HealthStatusResponse hs = createHealthStatusResponse(json);

            String tkNameId = hs.getTelemetry().getAppKey();
            ResponseInterpretation interpretation = hs.isEnabled() ? SUCCESS : FAILURE;
            List<String> messages = new ArrayList<>();
            if (!hs.isEnabled()) {
                messages.add("telemetry not enabled");
            }

            // create success, add tkNameId into properties
            RestServiceResponse restServiceResponse = new RestServiceResponse(uri, statusCode, interpretation, messages, -1);
            restServiceResponse.addProperty("tkNameId", tkNameId);
            addStackProperties(hs.getTelemetry().getStackVersion(), restServiceResponse);

            return restServiceResponse;
        } catch (Exception e) {
            String actMsg = "Could not query uri: " + uri;
            String exMsg = ExceptionUtils.getStackTrace(e);
            LOG.error(actMsg, e);
            return RestServiceResponse.createErrorRestServiceResponse(uri, -10, actMsg, exMsg);
        }
    }

    private void addStackProperties(String stackVersionFromTelemetry, RestServiceResponse restServiceResponse) {
        if (stackVersionFromTelemetry != null && !stackVersionFromTelemetry.isEmpty()) {
            Matcher regexMatcher = Pattern.compile("(?<stackName>.+) (?<stackVersion>.+)").matcher(stackVersionFromTelemetry);
            if (regexMatcher.matches()) {
                String stackName = regexMatcher.group("stackName");
                if (stackName.equals("JES")) {
                    stackName = "JES2";
                }
                restServiceResponse.addProperty("stackName", stackName);
                restServiceResponse.addProperty("stackVersion", regexMatcher.group("stackVersion"));
            }
        }
    }

    private String extractVersionNumber(String stackVersion) {
        if (stackVersion == null || stackVersion.isEmpty()) {
            return null;
        }
        return stackVersion.replaceAll("JES ", "");
    }

    HealthStatusResponse createHealthStatusResponse(String json) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        return mapper.readValue(json, HealthStatusResponse.class);
    }
}

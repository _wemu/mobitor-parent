package ch.mobi.mobitor.plugin.rest.service.client;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.http.client.fluent.Content;
import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.Test;

import ch.mobi.mobitor.plugin.rest.domain.swagger.ApiSpecResponse;

class OpenApiYamlHandlingStrategyTest {

    @Test
    void shouldDeserializeOpenAPIYaml() throws IOException {
        //-- arrange
        OpenApiYamlHandlingStrategy sut = new OpenApiYamlHandlingStrategy();
        //-- act
        String responseContentString = "openapi: 3.0.1\n" + "info:\n" + "  title: vvn-kaufen-b2c-service\n" + "  version: \"1\"\n" + "paths:\n"
                   + "  /angebote/versicherungsnehmer:\n" + "    put:\n" + "      summary: Ändern von Versicherungsnehmerdaten";
        ApiSpecResponse apiSpecResponse =
                sut.deserializeResponse(new Content(responseContentString.getBytes(StandardCharsets.UTF_8), ContentType.APPLICATION_OCTET_STREAM));
        //-- assert
        assertThat(apiSpecResponse.getOpenApiVersion()).isEqualTo("3.0.1");
    }

    /**
     * When the content type is "octet-stream" the charset is null. In org.apache.http.client.fluent.Content#asString(java.nio.charset.Charset)
     * ISO_8859_1 is used by default. ch.mobi.mobitor.plugin.rest.service.client.OpenApiYamlHandlingStrategy#deserializeResponse(org.apache.http.client.fluent.Content)
     * must use UTF-8 by default. Otherwise this leads to a "com.fasterxml.jackson.dataformat.yaml.JacksonYAMLParseException:
     * special characters are not allowed" when "Umlaute" (Ä) are present in the OpenAPI spec.
     */
    @Test
    void shouldNotUse_ISO_8859_1() throws IOException {
        //-- arrange
        OpenApiYamlHandlingStrategy sut = new OpenApiYamlHandlingStrategy();
        //-- act
        String responseContentString = "openapi: 3.0.1\n" + "info:\n" + "  title: vvn-kaufen-b2c-service\n" + "  version: \"1\"\n" + "paths:\n"
                   + "  /angebote/versicherungsnehmer:\n" + "    put:\n" + "      summary: Ändern von Versicherungsnehmerdaten";
        sut.deserializeResponse(new Content(responseContentString.getBytes(StandardCharsets.UTF_8), ContentType.APPLICATION_OCTET_STREAM));
        //-- assert
        // should not throw a com.fasterxml.jackson.dataformat.yaml.JacksonYAMLParseException
    }

}
package ch.mobi.mobitor.plugin.rest.service.configservice;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.ConfigDomainMapping;
import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.rest.RestPluginConfiguration;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.DefaultResourceLoader;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class DefaultEnvironmentsConfigurationServiceTest {

    private DefaultEnvironmentsConfigurationService environmentsConfigurationService;
    private RestPluginConfiguration restPluginConfiguration;

    @BeforeEach
    public void setup() {
        restPluginConfiguration = new RestPluginConfiguration();
        environmentsConfigurationService = new DefaultEnvironmentsConfigurationService(new DefaultResourceLoader(), restPluginConfiguration);

        // act
        environmentsConfigurationService.initializeEnvironments();
    }

    @Test
    public void appServersAreNotEmpty() {
        // assert
        List<EnvironmentConfigProperties> environmentConfigs = environmentsConfigurationService.getEnvironments();
        assertThat(environmentConfigs).isNotEmpty();
    }

    @Test
    public void testEnvironmentNetworkIsNeverNull() {
        List<EnvironmentConfigProperties> environmentConfigs = environmentsConfigurationService.getEnvironments();
        environmentConfigs.forEach(config -> assertThat(config.getNetwork()).isNotNull());
    }

    @Test
    public void testConfigDomainMappingsAreNeverNullForNonBuildEnvironments() {
        List<EnvironmentConfigProperties> environmentConfigs = environmentsConfigurationService.getEnvironments();

        environmentConfigs.stream().filter(config -> !config.isBuildEnvironment()).forEach(config -> {

            List<ConfigDomainMapping> configDomainMappings = config.getConfigDomainMappings();

            assertThat(configDomainMappings).isNotNull();
            assertThat(configDomainMappings).isNotEmpty();
        });
    }

    @Test
    public void testNamesAreNeverNullAndEndWithEnvLetter() {
        List<EnvironmentConfigProperties> environmentConfigs = environmentsConfigurationService.getEnvironments();
        environmentConfigs.forEach(config -> assertThat(config.getEnvironment()).isNotNull());
        environmentConfigs.forEach(config -> assertThat(config.getName()).isNotNull());
        environmentConfigs.forEach(config -> assertThat(config.getName().endsWith(config.getEnvironment().toLowerCase())).isTrue());
    }

    @Test
    public void testIsEnvironmentReachableWhenInLocalhost() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.LOCALHOST);

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("build"));

        assertTrue(environmentsConfigurationService.isEnvironmentReachable("Y")); // for local development, to see some rest calls
        assertFalse(environmentsConfigurationService.isEnvironmentReachable("W"));
        assertFalse(environmentsConfigurationService.isEnvironmentReachable("V"));

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("I"));
        assertFalse(environmentsConfigurationService.isEnvironmentReachable("T"));

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("P"));
    }

    @Test
    public void testIsEnvironmentReachableWhenInCi() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.CI);

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("build"));

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("Y"));
        assertFalse(environmentsConfigurationService.isEnvironmentReachable("W"));
        assertFalse(environmentsConfigurationService.isEnvironmentReachable("V"));

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("I"));
        assertFalse(environmentsConfigurationService.isEnvironmentReachable("T"));

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("P"));
    }

    @Test
    public void testIsEnvironmentReachableWhenInDevelopment() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.DEVELOPMENT);

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("build"));

        assertTrue(environmentsConfigurationService.isEnvironmentReachable("W"));
        assertTrue(environmentsConfigurationService.isEnvironmentReachable("Y"));
        assertTrue(environmentsConfigurationService.isEnvironmentReachable("V"));

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("I"));
        assertFalse(environmentsConfigurationService.isEnvironmentReachable("T"));

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("P"));
    }

    @Test
    public void testIsEnvironmentReachableWhenInPreProd() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.PREPROD);

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("build"));

        assertTrue(environmentsConfigurationService.isEnvironmentReachable("V"));
        assertTrue(environmentsConfigurationService.isEnvironmentReachable("W"));
        assertTrue(environmentsConfigurationService.isEnvironmentReachable("Y"));

        assertTrue(environmentsConfigurationService.isEnvironmentReachable("I"));
        assertTrue(environmentsConfigurationService.isEnvironmentReachable("T"));

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("P"));
    }

    @Test
    public void testIsEnvironmentReachableWhenInProduction() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.PRODUCTION);

        assertFalse(environmentsConfigurationService.isEnvironmentReachable("build"));

        assertTrue(environmentsConfigurationService.isEnvironmentReachable("V"));
        assertTrue(environmentsConfigurationService.isEnvironmentReachable("W"));
        assertTrue(environmentsConfigurationService.isEnvironmentReachable("Y"));

        assertTrue(environmentsConfigurationService.isEnvironmentReachable("I"));
        assertTrue(environmentsConfigurationService.isEnvironmentReachable("T"));

        assertTrue(environmentsConfigurationService.isEnvironmentReachable("P"));
    }

    @Test
    public void testIsNetworkReachableWhenInLocalhost() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.LOCALHOST);

        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.CI));
        assertTrue(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.DEVELOPMENT));
        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PREPROD));
        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PRODUCTION));
    }

    @Test
    public void testIsNetworkReachableWhenInCi() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.CI);

        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.CI));
        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.DEVELOPMENT));
        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PREPROD));
        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PRODUCTION));
    }

    @Test
    public void testIsNetworkReachableWhenInDevelopment() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.DEVELOPMENT);

        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.CI));
        assertTrue(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.DEVELOPMENT));
        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PREPROD));
        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PRODUCTION));
    }

    @Test
    public void testIsNetworkReachableWhenInPreProd() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.PREPROD);

        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.CI));
        assertTrue(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.DEVELOPMENT));
        assertTrue(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PREPROD));
        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PRODUCTION));
    }
    @Test
    public void testIsNetworkReachableWhenInProduction() {
        restPluginConfiguration.setNetwork(EnvironmentNetwork.PRODUCTION);

        assertFalse(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.CI));
        assertTrue(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.DEVELOPMENT));
        assertTrue(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PREPROD));
        assertTrue(environmentsConfigurationService.isNetworkReachable(EnvironmentNetwork.PRODUCTION));
    }

    @Test
    public void testEnvPipelineIsNotEmpty() {
        environmentsConfigurationService.getEnvironments().forEach(env -> assertThat(env.getPipeline()).isNotNull());
    }

}

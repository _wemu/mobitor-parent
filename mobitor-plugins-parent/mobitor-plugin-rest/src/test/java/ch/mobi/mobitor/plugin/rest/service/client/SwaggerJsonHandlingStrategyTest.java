package ch.mobi.mobitor.plugin.rest.service.client;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import static org.assertj.core.api.Assertions.assertThat;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import org.apache.http.client.fluent.Content;
import org.apache.http.entity.ContentType;
import org.junit.jupiter.api.Test;

import ch.mobi.mobitor.plugin.rest.domain.swagger.ApiSpecResponse;

class SwaggerJsonHandlingStrategyTest {

    @Test
    void shouldDeserializeSwaggerJson() throws IOException {
        //-- arrange
        SwaggerJsonHandlingStrategy sut = new SwaggerJsonHandlingStrategy();
        //-- act
        String responseContentString = "{ \"swagger\" : \"2.0\" }";
        ApiSpecResponse apiSpecResponse =
                sut.deserializeResponse(new Content(responseContentString.getBytes(StandardCharsets.UTF_8), ContentType.APPLICATION_OCTET_STREAM));
        //-- assert
        assertThat(apiSpecResponse.getSwaggerVersion()).isEqualTo("2.0");
    }

}

package ch.mobi.mobitor.plugin.jira;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 - 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.jira.domain.JiraInformation;
import ch.mobi.mobitor.plugins.api.MobitorPluginLegendGenerator;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;

import java.util.List;

import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;

public class JiraInformationLegendGenerator implements MobitorPluginLegendGenerator {

    @Override
    public List<ApplicationInformationLegendWrapper> createSuccessList() {
        JiraInformation ji = new JiraInformation();
        ji.markAsUpdated();
        ji.setTotal(0);
        ji.setCriticalIssues(0);
        ji.setMajorIssues(0);

        ApplicationInformationLegendWrapper wrapper = new ApplicationInformationLegendWrapper("Filter without matching issues", ji);
        return singletonList(wrapper);
    }

    @Override
    public List<ApplicationInformationLegendWrapper> createErrorList() {
        JiraInformation jiCrit = new JiraInformation();
        jiCrit.markAsUpdated();
        jiCrit.setTotal(5);
        jiCrit.setCriticalIssues(3);
        jiCrit.setMajorIssues(2);

        JiraInformation jiMaj = new JiraInformation();
        jiMaj.markAsUpdated();
        jiMaj.setTotal(5);
        jiMaj.setCriticalIssues(0);
        jiMaj.setMajorIssues(5);

        ApplicationInformationLegendWrapper w1 = new ApplicationInformationLegendWrapper("Filter with critical issues", jiCrit);
        ApplicationInformationLegendWrapper w2 = new ApplicationInformationLegendWrapper("Filter with major issues", jiMaj);
        return asList(w1, w2);
    }
}

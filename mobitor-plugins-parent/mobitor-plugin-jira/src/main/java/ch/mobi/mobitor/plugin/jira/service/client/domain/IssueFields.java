package ch.mobi.mobitor.plugin.jira.service.client.domain;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.text.ParseException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IssueFields implements Serializable {

    private static final Logger LOG = LoggerFactory.getLogger(IssueFields.class);

    @JsonProperty private String summary;
    @JsonProperty private IssueType issuetype;
    @JsonProperty private IssueUser creator;
    @JsonProperty private Date created;
    @JsonProperty private IssueUser reporter;
    @JsonProperty private IssuePriority priority;
    @JsonProperty private IssueResolution resolution;
    @JsonProperty private List<String> labels;
    @JsonProperty private Date resolutiondate;
    @JsonProperty private IssueUser assignee;
    @JsonProperty private Date updated;
    @JsonProperty private IssueStatus status;

    // custom fields, populated via AnySetter below
    @JsonIgnore
    private Map<String, Object> customFields = new HashMap<>();

    public String getSummary() {
        return summary;
    }

    public void setSummary(String summary) {
        this.summary = summary;
    }

    public IssueType getIssuetype() {
        return issuetype;
    }

    public void setIssuetype(IssueType issuetype) {
        this.issuetype = issuetype;
    }

    public IssueUser getCreator() {
        return creator;
    }

    public void setCreator(IssueUser creator) {
        this.creator = creator;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public IssueUser getReporter() {
        return reporter;
    }

    public void setReporter(IssueUser reporter) {
        this.reporter = reporter;
    }

    public IssuePriority getPriority() {
        return priority;
    }

    public void setPriority(IssuePriority priority) {
        this.priority = priority;
    }

    public IssueResolution getResolution() {
        return resolution;
    }

    public void setResolution(IssueResolution resolution) {
        this.resolution = resolution;
    }

    public List<String> getLabels() {
        return labels;
    }

    public void setLabels(List<String> labels) {
        this.labels = labels;
    }

    public Date getResolutiondate() {
        return resolutiondate;
    }

    public void setResolutiondate(Date resolutiondate) {
        this.resolutiondate = resolutiondate;
    }

    public IssueUser getAssignee() {
        return assignee;
    }

    public void setAssignee(IssueUser assignee) {
        this.assignee = assignee;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }

    public IssueStatus getStatus() {
        return status;
    }

    public void setStatus(IssueStatus status) {
        this.status = status;
    }

    public Set<String> getDeploymentEnvironments(String fieldName) {
        Object deploymentEnvs = this.customFields.get(fieldName);
        if (deploymentEnvs != null) {
            List<String> deplEnvList = (List<String>) deploymentEnvs;
            return new HashSet<>(deplEnvList);
        } else {
            return Collections.emptySet();
        }
    }

    public Date getDeploymentDate(String fieldName) {
        Object deploymentDate = this.customFields.get(fieldName);
        if (deploymentDate != null) {
            StdDateFormat df = new StdDateFormat();
            try {
                return df.parse((String) deploymentDate);

            } catch (ParseException e) {
                LOG.error("Could not parse deployment date: " + deploymentDate);
            }
        }

        return null;
    }

    @JsonAnySetter
    public void setCustomProperty(String name, Object value) {
        if(name != null && name.startsWith("customfield_")) {
            this.customFields.put(name, value);
        }
    }
}

package ch.mobi.mobitor.plugin.jira.service.client;

/*-
 * §
 * mobitor-plugin-jira
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.jira.service.client.domain.IssueResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.nio.file.Paths;

import static java.nio.file.Files.readAllBytes;
import static org.assertj.core.api.Assertions.assertThat;

public class IssueFieldsDeserializerTest {

    @Test
    public void testJiraResponseDeserializer() throws IOException, URISyntaxException {
        // arrange
        String deploymentEnvsKey = "customfield_10700";

        Path jiraCustomFieldsExampleFile = Paths.get(getClass().getClassLoader().getResource("jira-example-issue-with-customfield.json").toURI());
        String json = new String(readAllBytes(jiraCustomFieldsExampleFile));

        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        // act
        IssueResponse issue = mapper.readValue(json, IssueResponse.class);

        // assert
        assertThat(issue.getFields()).isNotNull();
        assertThat(issue.getFields().getDeploymentEnvironments(deploymentEnvsKey)).isNotNull();
        assertThat(issue.getFields().getDeploymentEnvironments(deploymentEnvsKey)).isNotEmpty();
        assertThat(issue.getFields().getDeploymentEnvironments(deploymentEnvsKey)).hasSize(1);
    }

}

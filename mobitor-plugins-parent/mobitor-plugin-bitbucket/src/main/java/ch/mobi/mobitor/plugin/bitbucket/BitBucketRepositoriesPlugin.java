package ch.mobi.mobitor.plugin.bitbucket;

/*-
 * §
 * mobitor-plugin-bitbucket
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.bitbucket.config.BitBucketRepositoryConfig;
import ch.mobi.mobitor.plugin.bitbucket.domain.BitBucketInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.bitBucketRepositories.enabled", havingValue = "true")
public class BitBucketRepositoriesPlugin implements MobitorPlugin<BitBucketRepositoryConfig> {

    private final BitBucketPluginConfiguration bitBucketPluginConfiguration;

    @Autowired
    public BitBucketRepositoriesPlugin(BitBucketPluginConfiguration bitBucketPluginConfiguration) {
        this.bitBucketPluginConfiguration = bitBucketPluginConfiguration;
    }

    @Override
    public String getConfigPropertyName() {
        return "bitBucketRepositories";
    }

    @Override
    public Class<BitBucketRepositoryConfig> getConfigClass() {
        return BitBucketRepositoryConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<BitBucketRepositoryConfig> configs) {
        for (BitBucketRepositoryConfig bbConfig : configs) {
            String serverName = bbConfig.getServerName();
            String applicationName = bbConfig.getApplicationName();
            String env = bbConfig.getEnvironment();
            String project = bbConfig.getProject();
            String repo = bbConfig.getRepository();

            BitBucketInformation information = new BitBucketInformation();
            information.setServerName(serverName);
            information.setApplicationName(applicationName);
            information.setEnvironment(env);
            information.setProject(project);
            information.setRepository(repo);

            information.setBitBucketProjectsUrl(bitBucketPluginConfiguration.getProjectsUrl());

            screen.addInformation(serverName, applicationName, env, information);
        }
    }

    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new BitBucketRepositoriesLegendGenerator().getLegendList();
    }
}

package ch.mobi.mobitor.plugin.sonarqube.service.client;

/*-
 * §
 * mobitor-plugin-sonarqube
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.sonarqube.service.client.domain.ProjectComponent;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.client.WireMock;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class SonarQubeClientTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

    private SonarQubeClient client;
    private SonarQubeConfiguration sonarConfiguration = new SonarQubeConfiguration();


    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    void setupEach() {
        String baseUrl = "http://localhost:" + wiremock.port();
        sonarConfiguration.setUsername("bitbucket-junit");
        sonarConfiguration.setPassword("bitbucket-junit");
        sonarConfiguration.setBaseUrl(baseUrl);

        client = new SonarQubeClient(sonarConfiguration);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    @Test
    public void retrieveProjectInformation() {
        // arrange
        final String projectKey = "ch.mobi.key";

        wiremock.stubFor(WireMock.get(WireMock.urlMatching("/sonarqube/api/measures/component?(.*)"))
                .withQueryParam("component", WireMock.equalTo(projectKey))
                .withHeader("Accept", WireMock.equalTo("application/json"))
                .withBasicAuth(sonarConfiguration.getUsername(), sonarConfiguration.getPassword())
                .willReturn(WireMock.aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("sonar-report-response-app1.json")));

        // act
        ProjectComponent projectComponent = client.retrieveProjectInformation(projectKey);

        // assert
        assertThat(projectComponent).isNotNull();
        assertThat(projectComponent.getId()).isNotNull();
        assertThat(projectComponent.getKey()).isNotNull();
        assertThat(projectComponent.getName()).isNotNull();
        assertThat(projectComponent.getDescription()).isNotNull();
        assertThat(projectComponent.getLineCoverage()).isNotEqualTo(0);
        assertThat(projectComponent.getOverallCoverage()).isNotEqualTo(0);
        assertThat(projectComponent.getDuplicatedBlocks()).isNotEqualTo(0);
        assertThat(projectComponent.getCriticals()).isNotEqualTo(0);
        assertThat(projectComponent.getId()).isNotEqualTo(0);
        assertThat(projectComponent.getCoverage()).isNotEqualTo(0);
        assertThat(projectComponent.getBlockers()).isNotEqualTo(0);
        assertThat(projectComponent.getItCoverage()).isNotEqualTo(0);
    }

}

package ch.mobi.mobitor.plugin.kubernetes.service.scheduling;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentNetwork;
import ch.mobi.mobitor.plugin.kubernetes.service.client.KubernetesClient;
import ch.mobi.mobitor.plugin.kubernetes.service.client.KubernetesPodInformationModel;
import ch.mobi.mobitor.plugin.kubernetes.service.client.domain.PodList;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

// TODO revive Pod Health Status with own config? Association to Liima Servers was not precise enough

@Component
public class KubernetesPodsInformationCollector {

    private final static Logger LOG = LoggerFactory.getLogger(KubernetesPodsInformationCollector.class);

    private final EnvironmentsConfigurationService environmentsConfigurationService;
    private final KubernetesClient kubernetesClient;
    private final KubernetesPodInformationModel kubernetesPodInformationModel;

    @Autowired
    public KubernetesPodsInformationCollector(EnvironmentsConfigurationService environmentsConfigurationService,
                                              KubernetesClient kubernetesClient,
                                              KubernetesPodInformationModel kubernetesPodInformationModel) {
        this.environmentsConfigurationService = environmentsConfigurationService;
        this.kubernetesClient = kubernetesClient;
        this.kubernetesPodInformationModel = kubernetesPodInformationModel;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.kubernetesPodsInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectPodsInformation() {
        for (EnvironmentNetwork network : EnvironmentNetwork.values()) {
            if (environmentsConfigurationService.isNetworkReachable(network)) {
                LOG.info("Collecting Pods Information for: " + network);

                PodList podList = kubernetesClient.retrievePodInformation(network);
                kubernetesPodInformationModel.addPodInformation(network, podList);

                LOG.info("Retrieved POD Information: " + (podList != null ? podList.getItems().size() : 0));

            } else {
                LOG.info("Network: " + network + " is not reachable from current network: " + environmentsConfigurationService.getCurrentNetwork());
            }
        }
    }

}

package ch.mobi.mobitor.plugin.kubernetes.domain;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;

public class JobCondition {

    @JsonProperty private String type; // "Complete"
    @JsonProperty private String status; // "True"
    @JsonProperty private LocalDateTime lastProbeTime;
    @JsonProperty private LocalDateTime lastTransitionTime;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public LocalDateTime getLastProbeTime() {
        return lastProbeTime;
    }

    public void setLastProbeTime(LocalDateTime lastProbeTime) {
        this.lastProbeTime = lastProbeTime;
    }

    public LocalDateTime getLastTransitionTime() {
        return lastTransitionTime;
    }

    public void setLastTransitionTime(LocalDateTime lastTransitionTime) {
        this.lastTransitionTime = lastTransitionTime;
    }
}

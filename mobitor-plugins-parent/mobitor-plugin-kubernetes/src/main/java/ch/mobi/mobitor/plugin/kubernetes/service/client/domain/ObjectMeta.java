package ch.mobi.mobitor.plugin.kubernetes.service.client.domain;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.Map;

/**
 * ObjectMeta is metadata that all persisted resources must have, which includes all objects users must create.
 *
 * @see <a href="https://kubernetes.io/docs/api-reference/v1.8/#objectmeta-v1-meta">ObjectMeta v1 meta</a>
 */
public class ObjectMeta {

    @JsonProperty private String name; // pod name, very dynamic thing
    @JsonProperty private String namespace;
    @JsonProperty private Map<String, String> annotations = new HashMap<>();

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNamespace() {
        return namespace;
    }

    public void setNamespace(String namespace) {
        this.namespace = namespace;
    }

    public Map<String, String> getAnnotations() {
        return annotations;
    }

    public void addAnnotation(String key, String value) {
        this.annotations.put(key, value);
    }
}

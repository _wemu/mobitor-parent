package ch.mobi.mobitor.plugin.kubernetes.service.scheduling;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.kubernetes.KubernetesPlugin;
import ch.mobi.mobitor.plugin.kubernetes.domain.KubernetesBatchJobInformation;
import ch.mobi.mobitor.plugin.kubernetes.service.client.KubernetesBatchJobInformationProviderService;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnBean;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.plugin.kubernetes.domain.KubernetesBatchJobInformation.KUBERNETES_BATCH_JOB;

@Component
@ConditionalOnBean(KubernetesPlugin.class)
public class KubernetesBatchJobInformationCollector {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final ScreensModel screensModel;
    private final RuleService ruleService;
    private final KubernetesBatchJobInformationProviderService kubernetesBatchJobInformationProviderService;

    @Autowired
    public KubernetesBatchJobInformationCollector(KubernetesBatchJobInformationProviderService kubernetesBatchJobInformationProviderService,
                                                  ScreensModel screensModel,
                                                  RuleService ruleService) {
        this.kubernetesBatchJobInformationProviderService = kubernetesBatchJobInformationProviderService;
        this.screensModel = screensModel;
        this.ruleService = ruleService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.kubernetesBatchJobInformationPollingInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectKubernetesBatchJobInformation() {
        long start = System.currentTimeMillis();
        logger.info("Started retrieving Kubernetes batch job information...");
        this.screensModel.getAvailableScreens().parallelStream().forEach(this::populateKubernetesBatchJobInformation);
        long stop = System.currentTimeMillis();
        logger.info("Reading Kubernetes batch job information took: " + (stop - start) + "ms");
    }

    private void populateKubernetesBatchJobInformation(Screen screen) {
        List<KubernetesBatchJobInformation> kubernetesBatchJobInformationList = screen.getMatchingInformation(KUBERNETES_BATCH_JOB);
        kubernetesBatchJobInformationList.forEach(kubernetesBatchJobInformationProviderService::updateBatchJobInformation);
        ruleService.updateRuleEvaluation(screen, KUBERNETES_BATCH_JOB);
        screen.setRefreshDate(KUBERNETES_BATCH_JOB, new Date());
    }
}

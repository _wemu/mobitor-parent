package ch.mobi.mobitor.plugin.kubernetes.service.client;

/*-
 * §
 * mobitor-plugin-kubernetes
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.apache.http.client.HttpClient;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.apache.http.impl.client.HttpClientBuilder;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;

@Component
public class KubernetesRestClient {

    private static final Logger LOG = LoggerFactory.getLogger(KubernetesRestClient.class);

    @Autowired
    public KubernetesRestClient() {
    }

    @Nullable
    public <T> T executeRequest(String url, Class<T> responseClass) {
        // use system properties, since the keystore contains the client certificate (liima / kubernetes)
        HttpClient httpClient = HttpClientBuilder.create()
                                                 .useSystemProperties()
                                                 .build();
        Executor executor = Executor.newInstance(httpClient);
        try {
            String response = executor
                    .execute(Request.Get(url)
                                    .addHeader("accept", "application/json")
                                    .connectTimeout(10000)
                                    .socketTimeout(10000))
                    .returnContent()
                    .asString();
            return createResponse(response, responseClass);
        } catch (Exception e) {
            LOG.error("Could not retrieve information from url: {}", url, e);
        }
        return null;
    }

    private <T> T createResponse(String json, Class<T> responseClass) throws IOException {
        ObjectMapper mapper = new ObjectMapper()
                .registerModule(new JavaTimeModule())
                .configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        T podsResponse = mapper.readValue(json, responseClass);
        return podsResponse;
    }
}

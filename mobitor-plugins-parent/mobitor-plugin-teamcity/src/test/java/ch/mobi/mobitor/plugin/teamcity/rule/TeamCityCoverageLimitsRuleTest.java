package ch.mobi.mobitor.plugin.teamcity.rule;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;

import static ch.mobi.mobitor.plugin.teamcity.domain.TeamCityCoverageInformation.TEAM_CITY_COVERAGE;
import static org.assertj.core.api.Assertions.assertThat;

public class TeamCityCoverageLimitsRuleTest extends PipelineRuleTest<TeamCityCoverageLimitsRule> {

    @Override
    protected TeamCityCoverageLimitsRule createNewRule() {
        return new TeamCityCoverageLimitsRule();
    }

    @Test
    public void evaluateRule_Ok() {
        // arrange
        TeamCityCoverageInformation teamCityCoverageInformation = new TeamCityCoverageInformation("config-id", "label");
        teamCityCoverageInformation.setCoverage(new BigDecimal("81.1"));

        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, teamCityCoverageInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation).isNotNull();
        assertThat(ruleEvaluation.hasErrors()).isFalse();
    }

    @Test
    public void evaluateRule_NotOk() {
        // arrange
        TeamCityCoverageInformation teamCityCoverageInformation = new TeamCityCoverageInformation("config-id", "label");
        teamCityCoverageInformation.setCoverage(new BigDecimal("80.9"));

        Pipeline pipeline = createPipeline();
        pipeline.addInformation(ENV, APP_NAME, teamCityCoverageInformation);

        // act
        RuleEvaluation ruleEvaluation = createNewRuleEvaluation();
        evaluateRule(pipeline, ruleEvaluation);

        // assert
        assertThat(ruleEvaluation).isNotNull();
        assertThat(ruleEvaluation.hasErrors()).isTrue();
    }

    @Test
    public void validatesType() {
        // arrange
        PipelineRule rule = createNewRule();

        // act
        boolean validatesType = rule.validatesType(TEAM_CITY_COVERAGE);

        // assert
        assertThat(validatesType).isTrue();
    }

}

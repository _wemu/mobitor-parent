package ch.mobi.mobitor.plugin.teamcity.service.client;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.containing;
import static com.github.tomakehurst.wiremock.client.WireMock.equalTo;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.matching;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;

class TeamCityClientTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

    private TeamCityClient client;

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    void setupEach() {
        String baseUrl = "http://localhost:" + wiremock.port() + "/TeamCity";

        TeamCityConfiguration configuration = new TeamCityConfiguration();
        configuration.setUsername("user");
        configuration.setPassword("password");
        configuration.setBaseUrl(baseUrl);

        client = new TeamCityClient(configuration);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    @BeforeEach
    public void setUp() {

    }

    @Test
    void retrieveBuildInformation_should_ask_only_defaultbranch() {
        // arrange
        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/builds.*"))
                .withQueryParam("locator", matching("buildType:([a-zA-Z0-9_\\-]+),running:any,failedToStart:any"))
                .withQueryParam("count", equalTo("1"))
                .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBody("teamcity-project-buildType-success.json")));

        // act
        client.retrieveBuildInformation("JUnitConfigId", false);

        // assert
        wiremock.verify(getRequestedFor(urlMatching("/TeamCity/httpAuth/app/rest/builds.*"))
                .withQueryParam("locator", equalTo("buildType:JUnitConfigId,running:any,failedToStart:any"))
                .withQueryParam("count", containing("1")));
    }

    @Test
    void retrieveBuildInformation_should_ask_anybranches() {
        // arrange
        wiremock.stubFor(get(urlMatching("/TeamCity/httpAuth/app/rest/builds.*"))
                        .withQueryParam("locator", matching("buildType:([a-zA-Z0-9_\\-\\,\\:]+)"))
                        .withQueryParam("count", equalTo("1"))
                        .willReturn(aResponse()
                                .withStatus(200)
                                .withHeader("Content-Type", "application/json")
                                .withBody("teamcity-project-buildType-success.json")));

        // act
        client.retrieveBuildInformation("JUnitConfigId", true);

        // assert
        wiremock.verify(getRequestedFor(urlMatching("/TeamCity/httpAuth/app/rest/builds.*"))
                .withQueryParam("locator", equalTo("buildType:JUnitConfigId,running:any,failedToStart:any,branch:default:any"))
                .withQueryParam("count", containing("1")));
    }

}

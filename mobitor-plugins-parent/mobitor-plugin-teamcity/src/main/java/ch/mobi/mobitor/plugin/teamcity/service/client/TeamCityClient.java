package ch.mobi.mobitor.plugin.teamcity.service.client;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.teamcity.service.client.domain.BuildsResponse;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.StatisticsResponse;
import ch.mobi.mobitor.plugin.teamcity.service.client.domain.project.ProjectBuildTypesResponse;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpHost;
import org.apache.http.client.fluent.Executor;
import org.apache.http.client.fluent.Request;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URI;

import static ch.mobi.mobitor.plugin.teamcity.service.scheduling.TeamCityBuildStatusCollector.CACHE_NAME_TEAMCITY_BUILDS;
import static ch.mobi.mobitor.plugin.teamcity.service.scheduling.TeamCityBuildStatusCollector.CACHE_NAME_TEAMCITY_BUILD_STATISTICS;
import static ch.mobi.mobitor.plugin.teamcity.service.scheduling.TeamCityBuildStatusCollector.CACHE_NAME_TEAMCITY_PROJECTS;
import static java.text.MessageFormat.format;

@Component
public class TeamCityClient {

    private static final Logger LOG = LoggerFactory.getLogger(TeamCityClient.class);

    private final TeamCityConfiguration teamCityConfiguration;

    @Autowired
    public TeamCityClient(TeamCityConfiguration teamCityConfiguration) {
        this.teamCityConfiguration = teamCityConfiguration;
    }

    @Cacheable(cacheNames = CACHE_NAME_TEAMCITY_BUILDS)
    public BuildsResponse retrieveBuildInformation(String configId, boolean showAnyBranch) {
        final String teamCityHost = teamCityConfiguration.getBaseUrl();
        final String username = teamCityConfiguration.getUsername();
        final String password = teamCityConfiguration.getPassword();

        // avoid handshake failure due to mixed up hostnames:
        // TODO verify this is still true: (see below!)
        System.setProperty("jsse.enableSNIExtension", "false");

        String requestUri = format("{0}/httpAuth/app/rest/builds?locator=buildType:{1},running:any,failedToStart:any", teamCityHost, configId);
        if (showAnyBranch) {
            requestUri = requestUri + ",branch:default:any";
        }
        requestUri = requestUri + "&count=1";

        LOG.debug("GET: " + requestUri);

        try {
            URI tcUri = new URI(teamCityHost);
            HttpHost host = HttpHost.create(tcUri.getHost());
            Executor executor = Executor.newInstance().auth(host, username, password).authPreemptive(host);

            String response = executor.execute(
                    Request.Get(requestUri)
                            .addHeader("accept", "application/json")
                            .connectTimeout(5000)
                            .socketTimeout(5000)
            )
                    .returnContent()
                    .asString();

            LOG.trace("JSON String: " + response);

            return createTeamCityResponse(response);

        } catch (Exception ex) {
            LOG.error("Could not retrieve build information for " + configId);
            LOG.error(configId + " " + ex.getMessage());
            LOG.debug("Exception", ex);
        }

        return null;
    }

    @Cacheable(cacheNames = CACHE_NAME_TEAMCITY_PROJECTS)
    public ProjectBuildTypesResponse retrieveProjectInformation(String projectId) {
        final String teamCityHost = teamCityConfiguration.getBaseUrl();
        final String username = teamCityConfiguration.getUsername();
        final String password = teamCityConfiguration.getPassword();

        // avoid handshake failure due to mixed up hostnames:
        System.setProperty("jsse.enableSNIExtension", "false");

        String requestUri = format("{0}/httpAuth/app/rest/projects/{1}/buildTypes", teamCityHost, projectId);
        LOG.debug("GET: " + requestUri);

        try {
            URI tcUri = new URI(teamCityHost);
            HttpHost host = HttpHost.create(tcUri.getHost());
            Executor executor = Executor.newInstance().auth(host, username, password).authPreemptive(host);

            String response = executor.execute(
                    Request.Get(requestUri)
                            .addHeader("accept", "application/json")
                            .connectTimeout(5000)
                            .socketTimeout(5000)
            )
                    .returnContent()
                    .asString();

            LOG.trace("JSON String: " + response);

            return createTeamCityProjectResponse(response);

        } catch (Exception ex) {
            LOG.error("Could not retrieve project information for " + projectId);
            LOG.error(projectId + " " + ex.getMessage());
            LOG.debug("Exception", ex);
        }

        return null;
    }

    private ProjectBuildTypesResponse createTeamCityProjectResponse(String jsonResponse) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        ProjectBuildTypesResponse projectBuildTypesResponse = mapper.readValue(jsonResponse, ProjectBuildTypesResponse.class);
        return projectBuildTypesResponse;
    }


    private BuildsResponse createTeamCityResponse(String jsonResponse) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        BuildsResponse buildsResponse = mapper.readValue(jsonResponse, BuildsResponse.class);
        return buildsResponse;
    }

    @Cacheable(cacheNames = CACHE_NAME_TEAMCITY_BUILD_STATISTICS)
    public StatisticsResponse retrieveBuildStatistics(String id) {
        final String teamCityHost = teamCityConfiguration.getBaseUrl();
        final String username = teamCityConfiguration.getUsername();
        final String password = teamCityConfiguration.getPassword();

        // /TeamCity/httpAuth/app/rest/buildTypes
        // -Djsse.enableSNIExtension=false
        // avoid handshake failure due to mixed up hostnames:
        System.setProperty("jsse.enableSNIExtension", "false");

        // /TeamCity/app/rest/builds/id:447908/statistics
        String requestUri = format("{0}/httpAuth/app/rest/builds/id:{1}/statistics", teamCityHost, id);
        LOG.debug("GET: " + requestUri);

        try {
            URI tcUri = new URI(teamCityHost);
            HttpHost host = HttpHost.create(tcUri.getHost());
            Executor executor = Executor.newInstance().auth(host, username, password).authPreemptive(host);

            String response = executor
                    .execute(
                            Request.Get(requestUri)
                                    .addHeader("accept", "application/json")
                                    .connectTimeout(5000)
                                    .socketTimeout(5000)
                    )
                    .returnContent()
                    .asString();

            LOG.trace("JSON String: " + response);

            return createTeamCityStatisticsResponse(response);

        } catch (Exception ex) {
            LOG.error("Could not load build statistics from TeamCity.", ex);
        }

        return null;

    }

    private StatisticsResponse createTeamCityStatisticsResponse(String jsonResponse) throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        StatisticsResponse stats = mapper.readValue(jsonResponse, StatisticsResponse.class);
        return stats;
    }

    public String getProjectWebUrl(String projectId) {
        return teamCityConfiguration.getBaseUrl() + "/project.html?projectId=" + projectId;
    }

}

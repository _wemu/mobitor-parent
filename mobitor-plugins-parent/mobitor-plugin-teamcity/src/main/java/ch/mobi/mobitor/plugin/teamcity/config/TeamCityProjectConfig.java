package ch.mobi.mobitor.plugin.teamcity.config;

/*-
 * §
 * mobitor-plugin-teamcity
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugins.api.PluginConfig;
import com.fasterxml.jackson.annotation.JsonProperty;

public class TeamCityProjectConfig extends PluginConfig {

    @JsonProperty
    private String projectId;
    /**
     * typically one of: co, rc, sys, etc. Currently only used as indicator / label on the UI
     */
    @JsonProperty
    private String label;

    /**
     * Tell if you want to consider all the branch for build information
     **/
    @JsonProperty
    private boolean showAnyBranch = false;

    public String getProjectId() {
        return projectId;
    }

    public void setProjectId(String projectId) {
        this.projectId = projectId;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public boolean isShowAnyBranch() {
        return showAnyBranch;
    }

    public void setShowAnyBranch(boolean showAnyBranch) {
        this.showAnyBranch = showAnyBranch;
    }
}

package ch.mobi.mobitor.plugin.streamworks.service.scheduling;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation;
import ch.mobi.mobitor.plugin.streamworks.service.client.StreamworksLastRunStatus;
import ch.mobi.mobitor.plugin.streamworks.service.client.StreamworksLastRunStatusService;
import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksJob;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.plugins.api.service.CollectorMetricService;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Java6Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static java.util.Arrays.asList;

import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunState.*;
import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation.STREAMWORKS_LAST_RUN_STATUS;

public class StreamworksLastRunStatusInformationCollectorTest {

    private final StreamworksJob streamworksJob = new StreamworksJob(
            "pdv_V_partnerDetektionJob",
            1l,
            "0",
            "Completed",
            "2017-06-27 12:37:43",
            "2017-06-27 12:37:57",
            "DIVE_V_PARHKx-S020",
        "http://testlink.com"
    );

    private StreamworksLastRunStatus pdvHousekeepingStatus= new StreamworksLastRunStatus(
            "pdv-partner-housekeeping-javabatch",
            "Completed",
            new ArrayList<>(Arrays.asList(streamworksJob))

    );

    @Test
    public void collectStreamworksLastRunStatusInformation(){
        // arrange
        RuleService ruleService = mock(RuleService.class);
        ScreensModel screensModel = mock(ScreensModel.class);
        StreamworksLastRunStatusService streamworksLastRunStatusService = mock(StreamworksLastRunStatusService.class);
        Screen screen = mock(Screen.class);
        List<Screen> screenList = singletonList(screen);
        StreamworksLastRunStatusInformation info1 = new StreamworksLastRunStatusInformation("mps-core-javabatch","MPS Javabatch", "Y", "exampleLink");

        when(screensModel.getAvailableScreens()).thenReturn(screenList);
        when(screen.getMatchingInformation(eq(STREAMWORKS_LAST_RUN_STATUS))).thenReturn(asList(info1));

        String state = "Completed";
        StreamworksLastRunStatus ed = pdvHousekeepingStatus;
        when(streamworksLastRunStatusService.retrieveStreamworksInformation(any(), any())).thenReturn(ed);

        CollectorMetricService collectorMetricService = mock(CollectorMetricService.class);
        StreamworksLastRunStatusInformationCollector collector = new StreamworksLastRunStatusInformationCollector(screensModel, ruleService, streamworksLastRunStatusService, collectorMetricService);

        //act
        collector.collectStreamworksLastRunStatusInformation();

        //assert
        verify(ruleService).updateRuleEvaluation(any(), eq(STREAMWORKS_LAST_RUN_STATUS));
        verify(screen).setRefreshDate(eq(STREAMWORKS_LAST_RUN_STATUS), any(Date.class));
        verify(collectorMetricService).updateLastRunCompleted(eq(STREAMWORKS_LAST_RUN_STATUS));
        verify(collectorMetricService).submitCollectorDuration(anyString(), anyLong());
        assertThat(info1.getState()).isEqualTo(COMPLETED);


    }
}

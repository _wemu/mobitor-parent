package ch.mobi.mobitor.plugin.streamworks.rule;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunState;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation;
import ch.mobi.mobitor.plugin.test.rule.PipelineRuleTest;
import org.junit.jupiter.api.Test;

import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation.STREAMWORKS_LAST_RUN_STATUS;
import static org.assertj.core.api.Assertions.assertThat;

public class StreamworksLastRunSuccessRuleTest extends PipelineRuleTest<StreamworksLastRunSuccessRule> {

    @Test
    public void validateRuleCaresAboutCorrectType() {
        // arrange
        StreamworksLastRunSuccessRule rule = new StreamworksLastRunSuccessRule();

        // act
        boolean validatesType = rule.validatesType(STREAMWORKS_LAST_RUN_STATUS);

        // assert
        assertThat(validatesType).isTrue();
    }

    @Test
    public void validateRuleHasFailuresWhenDeploymentFailed() {
        // arrange
        StreamworksLastRunSuccessRule rule = new StreamworksLastRunSuccessRule();
        Pipeline pipeline = createPipeline();
        StreamworksLastRunStatusInformation info = new StreamworksLastRunStatusInformation(SERVER_NAME, "Label", ENV, "url");
        info.setState(StreamworksLastRunState.ABNORMALLY_ENDED);

        pipeline.addInformation(ENV, APP_NAME, info);
        RuleEvaluation newRuleEvaluation = createNewRuleEvaluation();

        // act
        rule.evaluateRule(pipeline, newRuleEvaluation);

        // assert
        assertThat(newRuleEvaluation.hasErrors()).isTrue();
    }

    @Test
    public void validateRuleSuccessfulWhenDeploymentOk() {
        // arrange
        StreamworksLastRunSuccessRule rule = new StreamworksLastRunSuccessRule();
        Pipeline pipeline = createPipeline();
        StreamworksLastRunStatusInformation info = new StreamworksLastRunStatusInformation(SERVER_NAME,"Label", ENV, "url");
        info.setState(StreamworksLastRunState.COMPLETED);

        pipeline.addInformation(ENV, APP_NAME, info);
        RuleEvaluation newRuleEvaluation = createNewRuleEvaluation();

        // act
        rule.evaluateRule(pipeline, newRuleEvaluation);

        // assert
        assertThat(newRuleEvaluation.hasErrors()).isFalse();
    }

    @Override
    protected StreamworksLastRunSuccessRule createNewRule() {
        return new StreamworksLastRunSuccessRule();
    }
}

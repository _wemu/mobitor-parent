package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksJob;
import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksServerConfig;
import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.urlMatching;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class StreamworksLastRunStatusServiceTest {

    public static WireMockServer wiremock = new WireMockServer(WireMockConfiguration.options().dynamicPort());

    private StreamworksLastRunStatusService service;

    @BeforeAll
    static void setup() {
        wiremock.start();
    }

    @BeforeEach
    void setupEach() {
        String baseUrl = "http://localhost:" + wiremock.port() + SERVICE_BASE_PATH;
        configureLastRunUrl();

        StreamworksConfigurationService configService = mock(StreamworksConfigurationService.class);
        StreamworksServerConfig serverConfig = new StreamworksServerConfig(baseUrl, "V");
        when(configService.getStreamworksServerConfig(anyString())).thenReturn(serverConfig);
        service = new StreamworksLastRunStatusService(configService);
    }

    @AfterEach
    void after() {
        wiremock.resetAll();
    }

    @AfterAll
    static void clean() {
        wiremock.shutdown();
    }

    private final static String SERVICE_BASE_PATH = "/sworkswww/data/verarbeitungen/W";
    private final static String PDV_HOUSEKEEPING = "/pdv-partnerhousekeeping-batch";
    private final static String MPS_BATCH = "/mps-clearing-batch";


    private final StreamworksJob streamworksJob = new StreamworksJob(
            "pdv_V_partnerDetektionJob",
            1L,
            "0",
            "Completed",
            "2017-06-27 12:37:43",
            "2017-06-27 12:37:57",
            "DIVE_V_PARHKx-S020",
        "http://testlink.com"
    );

    private StreamworksLastRunStatus pdvHousekeepingStatus= new StreamworksLastRunStatus(
            "pdv-partnerhousekeeping-batch",
            "Running",
            new ArrayList<>(List.of(streamworksJob))

    );

    private StreamworksLastRunStatus mpsBatchStatus= new StreamworksLastRunStatus(
            "mps-clearing-batch",
            "Completed",
            null

    );

    private void configureLastRunUrl(){
        wiremock.stubFor(get(urlMatching("/.*")).willReturn(aResponse().withStatus(404)));

        wiremock.stubFor(get(urlMatching(SERVICE_BASE_PATH+PDV_HOUSEKEEPING+"/*"))
                        .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("streamworks-last-run-response.json")));

        wiremock.stubFor(get(urlMatching(SERVICE_BASE_PATH+MPS_BATCH+"/*"))
                        .willReturn(aResponse()
                        .withStatus(200)
                        .withHeader("Content-Type", "application/json")
                        .withBodyFile("streamworks-last-run-empty-response.json")));
    }

    @Test
    public void retrieveFromUnknownApplication() {
        // arrange
        // act
        StreamworksLastRunStatus runStatus = service.retrieveStreamworksInformation("xyz", "W");

        // assert
        assertNull( runStatus);
    }

    @Test
    public void retrieveFullInformation(){
        // arrange
        // act
        StreamworksLastRunStatus runStatus = service.retrieveStreamworksInformation("pdv-partnerhousekeeping-batch", "W");

        // act & assert
        assertEquals(pdvHousekeepingStatus, runStatus);
    }

    @Test
    public void retrieveEmptyState(){
        // arrange
        StreamworksLastRunStatus runStatus = service.retrieveStreamworksInformation("mps-clearing-batch", "W");

        // act & assert
        assertEquals(mpsBatchStatus, runStatus);
    }

}

package ch.mobi.mobitor.plugin.streamworks;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.streamworks.config.StreamworksLastRunConfig;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.junit.jupiter.api.Test;

public class StreamworksLastRunPluginTest {

  @Test
  public void createAndAssociateApplicationInformationBlocksShouldCreateStreamworksLastRunStatusInformation() {
    //given
    String serverName = "serverName";
    String applicationName = "applicationName";
    String streamworksApplicationName = "streamwork-application";
    String label = "label";
    String env = "Prod";
    String jbat3MonitorinUrl = "http://example";

    EnvironmentsConfigurationService environmentsConfigurationService = mock(
        EnvironmentsConfigurationService.class);

    StreamworksLastRunPlugin plugin = new StreamworksLastRunPlugin(
        environmentsConfigurationService);
    Screen screen = mock(Screen.class);
    ExtendableScreenConfig screenConfig = mock(ExtendableScreenConfig.class);
    StreamworksLastRunConfig config = new StreamworksLastRunConfig();
    config.setStreamworksApplicationName(streamworksApplicationName);
    config.setServerName(serverName);
    config.setApplicationName(applicationName);
    config.setLabel(label);
    config.setEnvironment(env);
    config.setJbat3MonitorUrl(jbat3MonitorinUrl);

    given(screen.getEnvironments()).willReturn(singletonList(env));
    given(environmentsConfigurationService.isEnvironmentReachable(anyString())).willReturn(true);

    //when
    plugin.createAndAssociateApplicationInformationBlocks(screen, screenConfig,
        singletonList(config));

    //then
    verify(screen).addInformation(eq(serverName), eq(applicationName), eq(env),
        any(StreamworksLastRunStatusInformation.class));
  }

  @Test
  public void createStreamworksLastRunInformationShouldContainAllRequiredProperties() {
    //given
    EnvironmentsConfigurationService environmentsConfigurationService = mock(
        EnvironmentsConfigurationService.class);
    given(environmentsConfigurationService.isNetworkReachable(anyString())).willReturn(true);
    StreamworksLastRunPlugin plugin = new StreamworksLastRunPlugin(
        environmentsConfigurationService);
    String applicationName = "junitServer";
    String applicationLabel = "junitLabel";
    String environment = "junitEnv";
    String jbat3MonitorUrl = "http://extermalLink.com";

    //when
    StreamworksLastRunStatusInformation information = plugin.creatStreamWorksLastRunStatusInformation(
        applicationName, applicationLabel, environment, jbat3MonitorUrl);

    //then
    assertThat(information.getApplicationName()).isEqualTo(applicationName);
    assertThat(information.getLabel()).isEqualTo(applicationLabel);
    assertThat(information.getEnvironment()).isEqualTo(environment);
    assertThat(information.getJbat3MonitorUrl()).isEqualTo(jbat3MonitorUrl);
  }

    @Test
    public void getConfigClass() {
        // arrange
        EnvironmentsConfigurationService environmentsConfigurationService = mock(EnvironmentsConfigurationService.class);
        given(environmentsConfigurationService.isNetworkReachable(anyString())).willReturn(true);
        StreamworksLastRunPlugin plugin = new StreamworksLastRunPlugin(environmentsConfigurationService);

        // act
        Class<StreamworksLastRunConfig> configClass = plugin.getConfigClass();

        // assert
        assertThat(configClass).isEqualTo(StreamworksLastRunConfig.class);
    }
}

package ch.mobi.mobitor.plugin.streamworks.rule;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.*;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation;

import java.util.List;
import java.util.Map;

public class StreamworksLastRunSuccessRule implements PipelineRule {
    @Override
    public void evaluateRule(Pipeline pipeline, RuleEvaluation newRuleEvaluation) {
        Map<String, ServerContext> serverContextMap = pipeline.getServerContextMap();
        for (Map.Entry<String, ServerContext> entry: serverContextMap.entrySet()){
            String env = entry.getKey();
            ServerContext serverContext = entry.getValue();

            List<StreamworksLastRunStatusInformation> streamworksLastRunStatusInformationList = serverContext.getMatchingInformation(StreamworksLastRunStatusInformation.STREAMWORKS_LAST_RUN_STATUS);
            streamworksLastRunStatusInformationList.stream().filter(this::ruleViolated).forEach(si -> newRuleEvaluation.addViolation(env, si, RuleViolationSeverity.ERROR, "Last Streamwork Job not successful."));

        }

    }

    private boolean ruleViolated(StreamworksLastRunStatusInformation streamworksLastRunStatusInformation){
        return !(streamworksLastRunStatusInformation.isSuccessful() || streamworksLastRunStatusInformation.isRunning());
    }

    @Override
    public boolean validatesType(String type) {
        return StreamworksLastRunStatusInformation.STREAMWORKS_LAST_RUN_STATUS.equals(type);
    }
}

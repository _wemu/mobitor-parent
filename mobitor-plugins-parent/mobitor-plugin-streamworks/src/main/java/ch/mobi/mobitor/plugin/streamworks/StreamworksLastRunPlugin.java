package ch.mobi.mobitor.plugin.streamworks;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.streamworks.config.StreamworksLastRunConfig;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@ConditionalOnProperty(name = "mobitor.plugins.streamworkslastrun.enabled", havingValue = "true")
public class StreamworksLastRunPlugin implements MobitorPlugin<StreamworksLastRunConfig> {

    private final EnvironmentsConfigurationService environmentsConfigurationService;

    @Autowired
    public StreamworksLastRunPlugin(EnvironmentsConfigurationService environmentsConfigurationService) {
        this.environmentsConfigurationService = environmentsConfigurationService;
    }

    @Override
    public String getConfigPropertyName() {
        return "streamworksLastRunStatus";
    }

    @Override
    public Class<StreamworksLastRunConfig> getConfigClass() {
        return StreamworksLastRunConfig.class;
    }

    @Override
    public void createAndAssociateApplicationInformationBlocks(Screen screen, ExtendableScreenConfig screenConfig, List<StreamworksLastRunConfig> configs) {

        for (StreamworksLastRunConfig config : configs) {
            String streamworksApplicationName = config.getStreamworksApplicationName();
            String label = config.getLabel();
            String env = config.getEnvironment();
            String jbat3MonitorUrl = config.getJbat3MonitorUrl();

            StreamworksLastRunStatusInformation info = creatStreamWorksLastRunStatusInformation(streamworksApplicationName, label, env, jbat3MonitorUrl);
            screen.addInformation(config.getServerName(), config.getApplicationName(), env, info);
        }
    }


    @Override
    public List<ApplicationInformationLegendWrapper> getLegendApplicationInformationList() {
        return new StreamworksLastRunLegendGenerator().getLegendList();
    }


    StreamworksLastRunStatusInformation creatStreamWorksLastRunStatusInformation(String applicationName, String label, String environment, String jbat3MonitorUrl) {
        return new StreamworksLastRunStatusInformation(applicationName, label, environment, jbat3MonitorUrl);
    }
}

package ch.mobi.mobitor.plugin.streamworks.config;
/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugins.api.PluginConfig;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class StreamworksLastRunConfig extends PluginConfig {

    /** Label which will be shown in directly in the Mobitor */
    @JsonProperty
    private String label;


    /** ApplicationName which is mapped to Streamworks */
    @JsonProperty
    private String streamworksApplicationName;

    @JsonProperty
    private String jbat3MonitorUrl;

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getStreamworksApplicationName() {
        return streamworksApplicationName;
    }

    public void setStreamworksApplicationName(String streamworksApplicationName) {
        this.streamworksApplicationName = streamworksApplicationName;
    }

    public String getJbat3MonitorUrl() {
        return jbat3MonitorUrl;
    }

    public void setJbat3MonitorUrl(String jbat3MonitorUrl) {
        this.jbat3MonitorUrl = jbat3MonitorUrl;
    }
}

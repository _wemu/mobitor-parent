package ch.mobi.mobitor.plugin.streamworks.service.client;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.WebClients;
import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksJob;
import ch.mobi.mobitor.plugin.streamworks.service.client.dto.StreamworksServerConfig;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import java.io.IOException;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import reactor.util.annotation.Nullable;

@Service
public class StreamworksLastRunStatusService {

  private static final Logger LOG = LoggerFactory.getLogger(StreamworksLastRunStatusService.class);


  private final StreamworksConfigurationService configurationService;

  @Autowired
  public StreamworksLastRunStatusService(StreamworksConfigurationService configurationService) {
    this.configurationService = configurationService;
  }

  @Nullable
  public StreamworksLastRunStatus retrieveStreamworksInformation(String applicationName,
      String environment) {
    StreamworksServerConfig config = configurationService.getStreamworksServerConfig(environment);

    if (config != null) {
      String url = String.format("%s/%s/", config.getUrl(), applicationName);
      String result;

      try {
        result = WebClients.withKeyStore().get()
            .uri(url)
            .retrieve()
            .bodyToMono(String.class)
            .block();
        return mapStreamWorksLastRunStatus(result);

      } catch (IOException ex) {
        LOG.error(
            "Could not retrieve Streamworks Response from URL {} with the following exception {}",
            url, ex);
      } catch (WebClientResponseException ex) {
        LOG.error(
            "Fetching StreamworksLastRunStatus from {} led to error with status code {}",
            url, ex.getRawStatusCode(), ex);
      }
    }
    return null;
  }

  private StreamworksLastRunStatus mapStreamWorksLastRunStatus(String json) throws IOException {
    ObjectMapper mapper = new ObjectMapper();

    JsonNode jobsNode = mapper.readTree(json).path("Jobs");
    final String verarbeitung = mapper.readTree(json).path("Verarbeitung").asText();
    final String status = mapper.readTree(json).path("Status").asText();
    List<StreamworksJob> streamworksJobs = null;

    ObjectReader reader = mapper.readerFor(new TypeReference<List<StreamworksJob>>() {
    });

    if (jobsNode != null && "ARRAY".equals(jobsNode.getNodeType().toString())) {
      streamworksJobs = reader.readValue(jobsNode);
    }

    return new StreamworksLastRunStatus(verarbeitung, status, streamworksJobs);
  }

}

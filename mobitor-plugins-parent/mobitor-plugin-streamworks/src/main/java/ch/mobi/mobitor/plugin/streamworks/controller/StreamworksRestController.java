package ch.mobi.mobitor.plugin.streamworks.controller;

/*-
 * §
 * mobitor-plugin-streamworks
 * --
 * Copyright (C) 2019 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation;
import ch.mobi.mobitor.plugin.streamworks.domain.StreamworksStatusInformation;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksLastRunStatusInformation.STREAMWORKS_LAST_RUN_STATUS;
import static ch.mobi.mobitor.plugin.streamworks.domain.StreamworksStatusInformation.STREAMWORKS_STATUS;

@Controller
public class StreamworksRestController {

    private final ScreensModel screensModel;

    @Autowired
    public StreamworksRestController(ScreensModel screensModel) {
        this.screensModel = screensModel;
    }

    @RequestMapping("/streamworks")
    public String overview(@RequestParam(value = "screen") String screenConfigKey,
                           @RequestParam(value = "env") String env,
                           @RequestParam(value = "server") String server,
                           @RequestParam(value = "application") String application,
                           Model model) {

        Screen screen = screensModel.getScreen(screenConfigKey);

        model.addAttribute("screenConfigKey", screenConfigKey);
        model.addAttribute("environment", env);
        model.addAttribute("serverName", server);
        model.addAttribute("applicationName", application);
        model.addAttribute("screen", screen);

        List<StreamworksLastRunStatusInformation> streamworksLastRunStatusList = screen.getMatchingInformation(STREAMWORKS_LAST_RUN_STATUS, env, server, application);
        model.addAttribute("streamworksLastRuns", streamworksLastRunStatusList);
        return "streamworksdetails";
    }


    @RequestMapping("/streamworksstatusdetails")
    public String overviewStatusDetails(@RequestParam(value = "screen") String screenConfigKey,
                                        @RequestParam(value = "env") String env,
                                        @RequestParam(value = "server") String server,
                                        @RequestParam(value = "application") String application,
                                        Model model) {

        Screen screen = screensModel.getScreen(screenConfigKey);

        model.addAttribute("screenConfigKey", screenConfigKey);
        model.addAttribute("environment", env);
        model.addAttribute("serverName", server);
        model.addAttribute("applicationName", application);
        model.addAttribute("screen", screen);

        List<StreamworksStatusInformation> streamworksStatusList = screen.getMatchingInformation(STREAMWORKS_STATUS, env, server, application);
        model.addAttribute("streamworksRuns", streamworksStatusList);
        return "streamworksStatusDetails";
    }
}

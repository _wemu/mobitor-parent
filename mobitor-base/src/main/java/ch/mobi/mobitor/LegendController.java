package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.DefaultPipeline;
import ch.mobi.mobitor.domain.screen.DefaultScreen;
import ch.mobi.mobitor.domain.screen.DefaultServerContext;
import ch.mobi.mobitor.model.ScreenViewHelper;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformationLegendWrapper;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyMap;

@Controller
public class LegendController {

    private final MobitorPluginsRegistry pluginsRegistry;
    private final ScreenViewHelper screenViewHelper;

    @Autowired
    public LegendController(MobitorPluginsRegistry pluginsRegistry,
                            ScreenViewHelper screenViewHelper) {
        this.pluginsRegistry = pluginsRegistry;
        this.screenViewHelper = screenViewHelper;
    }

    @RequestMapping("/legend")
    public String overview(Model model) {
        List<MobitorPlugin> plugins = pluginsRegistry.getConfigPlugins();


        Map<String, List<ApplicationInformationLegendWrapper>> legend = new HashMap<>();
        for (MobitorPlugin plugin : plugins) {
            String pluginConfigPropName = plugin.getConfigPropertyName();
            List<ApplicationInformationLegendWrapper> informationList = plugin.getLegendApplicationInformationList();

            legend.put(pluginConfigPropName, informationList);
        }

        model.addAttribute("legend", legend);
        model.addAttribute("svh", screenViewHelper);
        DefaultScreen legendScreen = new DefaultScreen.Builder()
                .configKey("LEGEND")
                .label("Legend Screen")
                .environments(asList("build", "test", "prod"))
                .serverNames(asList("legendServer1", "legendServer2"))
                .build();
        model.addAttribute("screen", legendScreen);
        Map<String, DefaultServerContext> serverContextMap = Map.of("build", new DefaultServerContext());
        DefaultPipeline legendPipeline = new DefaultPipeline("legendServer", serverContextMap);
        model.addAttribute("pipeline", legendPipeline);

        model.addAttribute("applicationName", "legendApplication1");

        return "legend";
    }

}

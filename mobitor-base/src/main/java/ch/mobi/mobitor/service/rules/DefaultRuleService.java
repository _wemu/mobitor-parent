package ch.mobi.mobitor.service.rules;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.DefaultRuleEvaluation;
import ch.mobi.mobitor.domain.screen.Pipeline;
import ch.mobi.mobitor.domain.screen.PipelineRule;
import ch.mobi.mobitor.domain.screen.RuleEvaluation;
import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.plugins.api.service.RuleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

import static ch.mobi.mobitor.domain.screen.RuleEvaluationSeverity.TROPHY;
import static ch.mobi.mobitor.domain.screen.RuleEvaluationSeverity.WARNINGS_ONLY;

@Service
public class DefaultRuleService implements RuleService {

    private final static Logger LOG = LoggerFactory.getLogger(DefaultRuleService.class);

    private List<PipelineRule> pipelineRules;

    @Autowired
    public DefaultRuleService(List<PipelineRule> pipelineRules) {
        this.pipelineRules = pipelineRules;
        LOG.info("Number of rules found: " + pipelineRules.size());

        for (PipelineRule pipelineRule : pipelineRules) {
            LOG.info("Will evaluate rule: " + pipelineRule.getClass().getName());
        }
    }

    public void updateRuleEvaluation(Screen screen, String type) {
        screen.getPipelines().forEach(pipeline -> updateRuleEvaluation(pipeline, type));
    }

    private void updateRuleEvaluation(Pipeline pipeline, String type) {
        pipeline.resetRuleEvaluation(type);

        pipelineRules.stream().filter(pipelineRule -> pipelineRule.validatesType(type)).forEach(pipelineRule -> {
            RuleEvaluation ruleEvaluation = new DefaultRuleEvaluation();
            pipelineRule.evaluateRule(pipeline, ruleEvaluation);
            if (ruleEvaluation.hasErrors() || ruleEvaluation.hasWarnings()) {
                pipeline.updateRuleEvaluation(type, ruleEvaluation);
            }
        });

        if (pipeline.getRuleEvaluationSeverity() == TROPHY || pipeline.getRuleEvaluationSeverity() == WARNINGS_ONLY) {
            pipeline.setLastRuleCompliantDate(new Date());
        }
    }
}

package ch.mobi.mobitor.service.scheduling;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.config.EnvironmentConfigProperties;
import ch.mobi.mobitor.model.EnvironmentTimestampModel;
import ch.mobi.mobitor.service.EnvironmentsConfigurationService;
import ch.mobi.mobitor.service.environment.EnvironmentTimestamps;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Component
public class EnvironmentTimestampCollector {

    private final EnvironmentTimestampModel environmentTimestampModel;
    private final EnvironmentsConfigurationService environmentsConfigurationService;

    private final RestTemplate restTemplate;

    @Autowired
    public EnvironmentTimestampCollector(EnvironmentTimestampModel environmentTimestampModel,
                                         RestTemplate restTemplate,
                                         EnvironmentsConfigurationService environmentsConfigurationService) {
        this.environmentTimestampModel = environmentTimestampModel;
        this.restTemplate = restTemplate;
        this.environmentsConfigurationService = environmentsConfigurationService;
    }

    @Scheduled(fixedDelayString = "${scheduling.pollingIntervalMs.environmentTimestampInterval}", initialDelayString = "${scheduling.pollingInitialDelayMs.second}")
    public void collectDb2Timestamp() {
        List<EnvironmentConfigProperties> environments = environmentsConfigurationService.getEnvironments();

        environments.stream()
                    .filter(environmentConfig -> StringUtils.isNotEmpty(environmentConfig.getTimestampUrl()))
                    .forEach(envConf -> {
                        String url = envConf.getTimestampUrl();
                        EnvironmentTimestamps envTimestamps = restTemplate.getForObject(url, EnvironmentTimestamps.class);
                        environmentTimestampModel.updateTimestamp(envConf.getEnvironment(), envTimestamps);
                    });
    }
}

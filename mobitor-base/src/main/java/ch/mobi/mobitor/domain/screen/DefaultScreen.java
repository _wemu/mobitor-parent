package ch.mobi.mobitor.domain.screen;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */


import ch.mobi.mobitor.plugins.api.domain.config.OnDutyConfig;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ApplicationInformation;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.*;

public class DefaultScreen implements Screen {

    @NotNull
    private String configKey;
    @NotNull
    private String label;
    // TODO plugin too? For sure no config class should be in here
    private OnDutyConfig onDuty;

    @NotEmpty
    private List<String> environments = new ArrayList<>();
    @NotEmpty
    private List<Pipeline> pipelines = new ArrayList<>();
    @NotNull
    private Map<String, Date> refreshDates = new HashMap<>();

    private DefaultScreen(String configKey, String label, List<String> environments, List<String> serverNames) {
        this.configKey = configKey;
        this.label = label;
        this.environments = environments;

        initializePipelines(serverNames, environments);
    }

    private Map<String, DefaultServerContext> createServerContextMap() {
        Map<String, DefaultServerContext> serverContextMap = new HashMap<>();
        for (String env : environments) {
            serverContextMap.put(env, new DefaultServerContext());
        }
        return serverContextMap;
    }

    private void initializePipelines(List<String> serverNames, List<String> environments) {
        for (String serverName : serverNames) {
            DefaultPipeline pipeline = new DefaultPipeline(serverName, createServerContextMap());
            this.addPipeline(pipeline);
        }
    }

    @Override
    public void addInformation(String serverName, String applicationName, String environment, ApplicationInformation information) {
        Pipeline pipeline = getAndInitializePipeline(serverName);
        pipeline.addInformation(environment, applicationName, information);
    }

    private Pipeline getAndInitializePipeline(String serverName) {
        for (Pipeline pipeline : this.pipelines) {
            if (serverName.equals(pipeline.getAppServerName())) {
                return pipeline;
            }
        }

        // be more forgiving, if pipeline is not yet here create it (in case plugins add pipelines later)
        // no pipeline found... create one then:
        DefaultPipeline pipeline = new DefaultPipeline(serverName, createServerContextMap());
        this.addPipeline(pipeline);

        return pipeline;
    }

    public OnDutyConfig getOnDuty() {
        return onDuty;
    }

    public void setOnDuty(OnDutyConfig onDuty) {
        this.onDuty = onDuty;
    }

    @Override
    public String getConfigKey() {
        return configKey;
    }

    @Override
    public String getLabel() {
        return label;
    }

    public void addEnvironments(List<String> environments) {
        this.environments.addAll(environments);
    }


    public List<Pipeline> getPipelines() {
        List<Pipeline> pipelines = Collections.unmodifiableList(this.pipelines);
        return pipelines;
    }

    @Override
    public List<String> getEnvironments() {
        return environments;
    }

    @Override
    public <T extends ApplicationInformation> List<T> getMatchingInformation(String type, String environment, String serverName, String applicationName) {
        for (Pipeline pipeline : this.pipelines) {
            if (serverName.equals(pipeline.getAppServerName())) {
                return (List<T>) pipeline.getMatchingInformation(type, environment, applicationName);
            }
        }
        return new ArrayList<>();
    }

    @Override
    public <T extends ApplicationInformation> List<T> getMatchingInformation(String type) {
        List<T> allInformationByTypeList = new ArrayList<>();
        pipelines.forEach(pipeline -> pipeline.getServerContextMap().values().forEach(srvCtx -> allInformationByTypeList.addAll(srvCtx.getMatchingInformation(type))));
        return allInformationByTypeList;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public void addPipeline(DefaultPipeline pipeline) {
        this.pipelines.add(pipeline);
    }

    public void setRefreshDate(String type, Date refreshDate) {
        refreshDates.put(type, refreshDate);
    }

    public Date getRefreshDate(String type) {
        return refreshDates.get(type);
    }

    public Map<String, Date> getRefreshDates() {
        return refreshDates;
    }

    public static class Builder {

        private String configKey;
        private String label;
        private List<String> environments;
        private List<String> serverNames;

        public Builder() {
        }

        public DefaultScreen build() {
            DefaultScreen screen = new DefaultScreen(configKey, label, environments, serverNames);
            return screen;
        }

        public Builder configKey(String configKey) {
            this.configKey = configKey;
            return this;
        }

        public Builder label(String label) {
            this.label = label;
            return this;
        }

        public Builder environments(List<String> environments) {
            this.environments = environments;
            return this;
        }

        public Builder serverNames(List<String> serverNames) {
            this.serverNames = serverNames;
            return this;
        }
    }
}

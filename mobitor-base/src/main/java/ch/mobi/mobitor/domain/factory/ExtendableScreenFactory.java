package ch.mobi.mobitor.domain.factory;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.domain.screen.DefaultScreen;
import ch.mobi.mobitor.plugins.api.MobitorPlugin;
import ch.mobi.mobitor.plugins.api.domain.config.ExtendableScreenConfig;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ExtendableScreenFactory {

    private static final Logger LOG = LoggerFactory.getLogger(ExtendableScreenFactory.class);


    private final MobitorPluginsRegistry pluginsRegistry;

    @Autowired
    public ExtendableScreenFactory(MobitorPluginsRegistry pluginsRegistry) {
        this.pluginsRegistry = pluginsRegistry;
    }

    public DefaultScreen initializeEmptyScreen(ExtendableScreenConfig screenConfig) {
        DefaultScreen screen = new DefaultScreen.Builder()
                .configKey(screenConfig.getConfigKey())
                .label(screenConfig.getLabel())
                .environments(screenConfig.getEnvironments())
                .serverNames(screenConfig.getServerNames())
                .build();
        screen.setOnDuty(screenConfig.getOnDuty()); // TODO replace by plugin or own Domain DTO - not the config directly

        Map<String, List> pluginConfigMap = screenConfig.getPluginConfigMap();
        for (Map.Entry<String, List> pluginConfigEntry : pluginConfigMap.entrySet()) {
            MobitorPlugin plugin = pluginsRegistry.getPlugin(pluginConfigEntry.getKey());
            if (plugin != null) {
                List configs = screenConfig.getPluginConfigMap().get(plugin.getConfigPropertyName());
                plugin.createAndAssociateApplicationInformationBlocks(screen, screenConfig, configs);
            }
        }

        LOG.debug("Screen creation based on plugins complete.");

        return screen;
    }

}

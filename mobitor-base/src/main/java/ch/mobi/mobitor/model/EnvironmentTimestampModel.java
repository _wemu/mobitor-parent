package ch.mobi.mobitor.model;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.service.environment.EnvironmentTimestamps;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class EnvironmentTimestampModel {
    private Map<String, EnvironmentTimestamps> envToTimestampMap = new HashMap<>();

    public void updateTimestamp(String environment, EnvironmentTimestamps environmentTimestamps) {
        envToTimestampMap.put(environment, environmentTimestamps);
    }

    public EnvironmentTimestamps getTimestamps(String environment) {
        return envToTimestampMap.get(environment);
    }

    public boolean hasTimestamps(String environment) {
        return envToTimestampMap.containsKey(environment);
    }
}

package ch.mobi.mobitor;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import ch.mobi.mobitor.domain.screen.Screen;
import ch.mobi.mobitor.model.EnvironmentInformationHelper;
import ch.mobi.mobitor.model.ScreenViewHelper;
import ch.mobi.mobitor.plugins.api.domain.screen.information.ScreenAttributeProvider;
import ch.mobi.mobitor.plugins.api.model.ScreensModel;
import ch.mobi.mobitor.service.OnDutyService;
import ch.mobi.mobitor.service.plugins.MobitorPluginsRegistry;

@Controller
public class ScreenController {

    private final ScreensModel screensModel;
    private final ScreenViewHelper screenViewHelper;
    private final OnDutyService onDutyService;
    private final EnvironmentInformationHelper environmentInformationHelper;
    private final MobitorPluginsRegistry pluginsRegistry;

    @Autowired
    public ScreenController(ScreensModel screensModel,
                            ScreenViewHelper screenViewHelper,
                            OnDutyService onDutyService,
                            EnvironmentInformationHelper environmentInformationHelper,
                            MobitorPluginsRegistry pluginsRegistry) {
        this.screensModel = screensModel;
        this.screenViewHelper = screenViewHelper;
        this.onDutyService = onDutyService;
        this.environmentInformationHelper = environmentInformationHelper;
        this.pluginsRegistry = pluginsRegistry;
    }

    @RequestMapping("/screen")
    public String overview(
            @RequestParam(value = "key", required = false, defaultValue = "1") String screenConfigKey,
            @RequestParam(value = "rotate", required = false, defaultValue = "") String rotate,
            @RequestParam(value = "refresh", required = false, defaultValue = "60") int refresh,
            @RequestParam(value = "zoom", required = false, defaultValue = "1") String zoom,
            Model model) {

        String nextScreenConfigKey = screenConfigKey;

        if (StringUtils.isNotBlank(rotate)) {
            String[] screenKeysToRotate = rotate.split(",");
            for (int i = 0; i < screenKeysToRotate.length; i++) {
                String screenKey = screenKeysToRotate[i];
                if (screenKey.equals(screenConfigKey)) {
                    // show next screen:
                    int pos = (i + 1) % screenKeysToRotate.length;
                    nextScreenConfigKey = screenKeysToRotate[pos];
                    break;
                }
            }
        }

        model.addAttribute("screenConfigKey", screenConfigKey);
        model.addAttribute("nextScreenConfigKey", nextScreenConfigKey);
        model.addAttribute("rotate", rotate);
        model.addAttribute("refresh", refresh);
        model.addAttribute("zoom", zoom);

        pluginsRegistry.getConfigPlugins().forEach(plugin -> {
            ScreenAttributeProvider screenAttributeProvider = plugin.getScreenAttributeProvider();
            Map<String, Object> attributes = screenAttributeProvider.getAttributes();
            model.addAllAttributes(attributes);
        });

        if (screensModel.hasScreen(screenConfigKey)) {
            Screen screen = screensModel.getScreen(screenConfigKey);
            if (screen == null) {
                return "nodata";

            } else {
                model.addAttribute("screen", screen);
                model.addAttribute("svh", screenViewHelper);
                model.addAttribute("eih", environmentInformationHelper);
                model.addAttribute("onDutyService", onDutyService);
                return "screen";
            }

        } else {
            return "invalidscreenconfig";
        }
    }

}

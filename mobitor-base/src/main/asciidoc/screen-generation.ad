= Screen generation

The usual approach to define what is shown on a screen is editing the screen.json file manually.

Yet there are cases where overall screens are required or screens that contain all components of a certain type and so
on. For this purpose the Mobitor allows you to implement a `ScreenConfigGenerator`.

The screen generator is then called with all the manually configured screens and can return a new
generated screen configuration. This is then passed like all other configurations into the screen
factory to create the screen model used by the services and the UI.

Examples for this are:
 * generating an overall screen that contains everything of other screens.
 * generate a screen with all servers that belong to a specific criteria

The generator needs to take care no duplicates are added to the generated screen and what
information blocks it wants to add.

We recommend to use uppercase letters for the screenConfigKey.

An overall screen generator example is part of the mobitor-webapp project.


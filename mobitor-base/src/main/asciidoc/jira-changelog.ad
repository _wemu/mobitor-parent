= Jira Changelog

Since the Mobitor collects data from BitBucket, Deployments it can determine if a commit was already deployed to an
environment. It can then create labels on a custom field that can be used to filter what changed on a specific environment
during a time period.


image::images/jira-changelog-labels.png[]

The image shows the custom field in jira (that is then configured in the Mobitor) that is used to add labels for each
environment the feature was deployed to.

== What you have to do:

 * Developers MUST use the Jira issue number in their commit messages to BitBucket.
 * every release created must also create a tag named: `release/<version>` where `<version>` is the exact same version
   used for deployment (currently there is no configuration setting for this).

Configure what repository in BitBucket contains which deployment:

.classpath:screen/changelog-repositories.json
[source, json]
----
[
  {
    "serverName": "server_name",
    "applicationName": "application_name",
    "project": "BITBUCKETPROJECT",
    "repository": "BITBUCKETGITREPONAME",
    "environments": [
      "B",
      "T",
      "P"
    ]
  }
]
----

`serverName` refers to the serverNames on each screen and is usually used to determine the deployed version. +
`applicationName` refers to the applicationName in a screen as well. +
`project` is the name of the BitBucket Server project (usually only a few uppercase letters, but case doesn't matter). +
`repository` is the name of the BitBucket Git repository that contains the sources of the deployment (no .git suffix needed)
`environments` is a list, usually a sub-set of the environments on a screen. These environments are checked for deployments
and then labeled should a jira issue be found.

The changelog module currently relies on the `environment-mappings.json` configuration file for the environment names.
This is part of the REST Plugin. It will use the "name" property for the labels.

The following properties can be set in the application-<profile>.properties file:

[source]
----
mobitor.plugins.jira.username=
mobitor.plugins.jira.password=
mobitor.plugins.jira.baseUrl=https://jira.host.domain

changelog.config.customFieldNameDeploymentEnvironments=customfield_102030
# This format is ISO 8601: YYYY-MM-DDThh:mm:ss.sTZD
changelog.config.customFieldNameDeploymentDatePreProd=customfield_111213
changelog.config.customFieldNameDeploymentDateProduction=customfield_141516
changelog.config.blacklistedProjects[0]=
changelog.config.blacklistedProjects[1]=
changelog.config.blacklistedProjects[2]=
changelog.config.blacklistedProjects[3]=
----

There are three custom field you need to add in Jira. One for the labels of the environments, two for the dates of preprod
and production (to simplify filtering by date).

The blacklistedProjects array contains a Jira project per line that should not be updated.

The changelog itself is then your own created Jira filter, tailored to whatever need you user base has.


== How it works

 * Mobitor will read the deployments on the configured environments and determine the version that is deployed there
 * It will then try to find a tag in BitBucket named `release/<version>`
 * If a tag is found it will look into the commit history if Jira issue keys are added to the commit messages
 * For the Jira issues found, it will then add the label for the matching environment

The issue state is not considered. If it is still open, parts of that feature did end up in a deployment.

There is a status page in Mobitor (<host>/mobitor/changelog) where you can check when the last update was.
This changelog is currently updated twice a day.

package ch.mobi.mobitor.domain.screen.information;

/*-
 * §
 * mobitor-base
 * --
 * Copyright (C) 2018 Die Mobiliar
 * --
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * §§
 */

import ch.mobi.mobitor.plugin.swd.domain.SwdDeploymentInformation;
import org.jetbrains.annotations.NotNull;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;

public class SwdDeploymentInformationTest {
    
    @Test
    public void patchDateShouldReturnFormattedDate() {
        //given
        SwdDeploymentInformation information = aSwdDeploymentInformation();
    
        //when
        String result = information.patchDate();

        //then
        assertThat(result).isEqualTo("13.12.2011 14:15:16");
    }

    @Test
    public void patchDateShouldReturnNullIfPatchDateIsNotSet() {
        //given
        SwdDeploymentInformation information = aSwdDeploymentInformation();
        information.setPatchDate(null);

        //when
        String result = information.patchDate();

        //then
        assertThat(result).isNull();
    }

    @NotNull
    public static SwdDeploymentInformation aSwdDeploymentInformation() {
        SwdDeploymentInformation information = new SwdDeploymentInformation("foo.zip", "P");
        information.setRevision("1234.");
        information.setPatchDate(LocalDateTime.of(2011, 12,13, 14, 15, 16));
        return information;
    }

    @Test
    public void testNullRevisionDoesNotKillVersion() {
        // arrange
        SwdDeploymentInformation information = new SwdDeploymentInformation("appName", "P");
        information.setRevision(null);

        // act
        String version = information.getVersion();

        // assert
        assertNull(version);
    }

    @Test
    public void testValidGitRevisionHasSixCharsVersion() {
        // arrange
        SwdDeploymentInformation information = new SwdDeploymentInformation("appName", "P");
        information.setRevision("392f7feba75c16afda4fbb67680f3373f7466ee3");

        // act
        String version = information.getVersion();

        // assert
        assertNotNull(version);
        assertThat(version).hasSize(6);
    }


}

# Mobitor

This is the base module of the Mobitor application (the Mobiliar Build Monitor).

This base module contains the core plugins, the plugin interfaces and the (empty) Spring Boot application.

To use or create your own build Mobitor please refer to the [website](https://mobiliar.bitbucket.io/mobitor/)
of the latest release.

The snapshot build creates a [snapshot-website](https://mobiliar.bitbucket.io/mobitor-snapshot/).

If you want **to get started the repository to clone** is [mobitor-webapp](https://bitbucket.org/mobiliar/mobitor-webapp/).


## Swagger-UI

The Mobitor contains a Swagger-UI by including Springfox. It is accessible via /mobitor/swagger-ui.html
The swagger.json resides below: /mobitor/rest/swagger.json


## Prometheus export

The application uses Spring Boot Actuator and the Prometheus exporter to publish some metrics.

For example the duration the collectors that retrieve information is exported or the cache statistics.

The prometheus metrics can be viewed at: `/mobitor/actuator/prometheus`
